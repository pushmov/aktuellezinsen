<?php 
	get_header();
	$custom = get_post_custom();
?>
<section class="ct-news ct-tagesgeld ct-page-custom">
	<div class="container">
		<div class="row">
			<?php if (!isset($custom['show_comparison_table']) || $custom['show_comparison_table'][0] == '0') : ?>
			<div class="col-lg-9">
			<?php else : ?>
			<div class="col-lg-12">
			<?php endif; ?>
				<div class="row">
					<div class="col-lg-12">
						<?php
						while ( have_posts() ) :
							the_post();
							get_template_part( 'template-parts/content-page-custom', 'page' );
							endwhile; // End of the loop.
						?>
					</div>
					<div class="col-lg-12">
						<div class="panel panel-default wissenswertes">
							<div class="panel-heading">
								<h3><?php echo $custom['title'][0]; ?></h3>
							</div>
					  		<div class="panel-body panel-news panel-aktuelle-news">
					  			<div class="body">
					  				<?php echo $custom['free_text_content'][0];?>
					  			</div>
					  		</div>
					  		<div class="panel-footer"></div>
						</div>
					</div>
				</div>
			</div>
			<?php if (!isset($custom['show_comparison_table']) || $custom['show_comparison_table'][0] == '0') : ?>
			<div class="col-lg-3">
				<?php get_sidebar('news'); ?>
			</div>
			<?php endif; ?>
		</div>
	</div>
</section>
<?php get_footer(); ?>