<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Aktuellezinsen.net
 */

get_header();
global $wpdb;
?>
	<section class="ct-body">
		<div class="container-fluid maxwidth-1">
			<div class="row">
				<div class="ct-menu">
					
					<div class="col-lg-8">
						<h2>Wir finden die Besten Zinsen</h2>
						<div class="text-center ico">
							<a href="<?php echo get_home_url(); ?>/festgeldzinsen/">
								<img src="<?php echo get_template_directory_uri(); ?>/includes/img/ico-piggy.png" >
								<h5>Festgeld</h5>
							</a>
						</div>
						<div class="text-center ico">
							<a href="<?php echo get_home_url(); ?>/tagesgeldzinsen/">
								<img src="<?php echo get_template_directory_uri(); ?>/includes/img/ico-calendar.png" >
								<h5>Tagesgeld</h5>
							</a>
						</div>
						<div class="text-center ico">
							<a href="<?php echo get_home_url(); ?>/ratenkreditzinsen/">
								<img src="<?php echo get_template_directory_uri(); ?>/includes/img/ico-hand.png" >
								<h5>Kredit</h5>
							</a>
						</div>
						<div class="text-center ico">
							<a href="<?php echo get_home_url(); ?>/baufinanzierungszinsen/">
								<img src="<?php echo get_template_directory_uri(); ?>/includes/img/ico-house.png" >
								<h5>Baufinanzierung</h5>
							</a>
						</div>
						<div class="text-center ico">
							<a href="<?php echo get_home_url(); ?>/kreditkartenzinsen/">
								<img src="<?php echo get_template_directory_uri(); ?>/includes/img/ico-card.png" >
								<h5>Kreditkarte</h5>
							</a>
						</div>
						<div class="text-center ico">
							<a href="<?php echo get_home_url(); ?>/girokontozinsen/">
								<img src="<?php echo get_template_directory_uri(); ?>/includes/img/ico-bank.png" >
								<h5>Girokonto</h5>
							</a>
						</div>
					</div>
					<div class="col-lg-3"></div>
				</div>
			</div>
		</div>
	</section>
	<section class="ct-top">
		<div class="container">
			<div class="row">
				<h1>Aktuelle Zinsen</h1>
				<?php

					$festgeld = $wpdb->get_results( "SELECT festgeld.festgeld_id,festgeld.bank_id,festgeld.48_month AS interest, banks.bank_id, banks.bank_name, banks.image, banks.link_festgeld FROM `festgeld` INNER JOIN banks ON banks.bank_id = festgeld.bank_id WHERE banks.status = 'ok' ORDER BY 48_month DESC LIMIT 0,5" );
				?>
				<div class="col-lg-4">
					<div class="panel panel-default">
						<div class="panel-heading text-center">
							<h3>Festgeld TOP 5</h3>
						</div>
		  				<div class="panel-body">
		  					<div class="header clearfix">
		  						<div class="anbieter pull-left">Anbieter</div>
		  						<div class="zinssatz pull-left">Zinssatz</div>
		  					</div>
		  					<div class="body">
		  						<?php if (!empty($festgeld)) :?>
		  						<?php foreach ($festgeld as $item) : ?>
		  						<div class="item clearfix">
			  						<div class="anbieter pull-left">
			  							<strong><?php echo str_replace('_', ' ', $item->bank_name); ?></strong>
			  						</div>
			  						<div class="zinssatz pull-left">
			  							<strong><?php echo number_format($item->interest, 2, ',', '.'); ?>%</strong>
			  							<a href="<?php echo get_home_url().$item->link_festgeld; ?>" onclick="console.log('a');_gaq.push(['_trackPageview', '/startseite/<?php echo $item->bank_name;?>/festgeld/']);return true;" title="zum Angebot der <?php echo str_replace('_', ' ', $item->bank_name); ?>" class="btn btn-primary btn-zinssatz"><img src="<?php echo get_template_directory_uri(); ?>/includes/img/chevron-right.png"></a>
			  						</div>
		  						</div>
		  						<?php endforeach; ?>
		  						<?php endif; ?>
		  						<p>Festgeldzinsen für 48 Monate</p>
		  						<div class="panel-body-footer text-right">
		  							<a href="<?php echo get_home_url() ?>/festgeldzinsen/" title="Aktuelle Festgeldzinsen" class="btn btn-default">Alle Anbieter anzeigen <i class="fa fa-chevron-right"></i></a>
		  						</div>
		  					</div>
		  				</div>
		  				<div class="panel-footer"></div>
					</div>
				</div>

				<?php

					$tagesgeld = $wpdb->get_results( "SELECT tagesgeld.tagesgeld_id,tagesgeld.bank_id,tagesgeld.Zinssatz_table, banks.bank_id, banks.bank_name, banks.image, banks.link_tagesgeld FROM `tagesgeld` INNER JOIN banks ON banks.bank_id = tagesgeld.bank_id WHERE banks.status = 'ok' ORDER BY Zinssatz_table DESC LIMIT 0,5" );
				?>
				<div class="col-lg-4">
					<div class="panel panel-default">
						<div class="panel-heading text-center">
							<h3>Tagesgeld TOP 5</h3>
						</div>
		  				<div class="panel-body">
		  					<div class="header clearfix">
		  						<div class="anbieter pull-left">Anbieter</div>
		  						<div class="zinssatz pull-left">Zinssatz</div>
		  					</div>
		  					<div class="body">
		  						<?php if (!empty($tagesgeld)) : ?>
		  						<?php foreach ($tagesgeld as $row) : ?>
		  						<div class="item clearfix">
			  						<div class="anbieter pull-left">
			  							<strong><?php echo str_replace('_', ' ', $row->bank_name);?></strong>
			  						</div>
			  						<div class="zinssatz pull-left">
			  							<strong><?php echo number_format($row->Zinssatz_table, 2, ',', '.'); ?>%</strong>
			  							<a href="<?php echo get_home_url().$row->link_tagesgeld;?>" title="zum Angebot der <?php echo str_replace('_', ' ', $row->bank_name); ?>" class="btn btn-primary btn-zinssatz" onclick="_gaq.push(['_trackPageview', '/startseite/<?php echo $row->bank_name;?>/tagesgeld']);return true;"><img src="<?php echo get_template_directory_uri(); ?>/includes/img/chevron-right.png"></a>
			  						</div>
		  						</div>
		  						<?php endforeach; ?>
		  						<?php endif; ?>
		  						<p>&nbsp;</p>
		  						<div class="panel-body-footer text-right">
		  							<a href="<?php echo get_home_url(); ?>/tagesgeldzinsen/" title="Aktuelle Zinsen Tagesgeld" class="btn btn-default">Alle Anbieter anzeigen <i class="fa fa-chevron-right"></i></a>
		  						</div>
		  					</div>
		  				</div>
		  				<div class="panel-footer"></div>
					</div>
				</div>

				<?php
					$banks = array();
					$link = $text = $widget_content = '';
					$sidebar_id = 'custom-top-5';
					$sidebars_widgets = wp_get_sidebars_widgets();
					$widget_ids = $sidebars_widgets[$sidebar_id]; 
					foreach( $widget_ids as $id ) {
						$wdgtvar = 'widget_'._get_widget_id_base( $id );
					    $idvar = _get_widget_id_base( $id );
					    $instance = get_option( $wdgtvar );
					    $idbs = str_replace( $idvar.'-', '', $id );
					    //$widget_content = $instance[$idbs]['content'];
					    $test[] = $instance[$idbs];
					    if (isset($instance[$idbs]['interest'])){
					    	$banks[] = $instance[$idbs];
					    }
					    if ($instance[$idbs]['title'] == 'text') {
					    	$text = $instance[$idbs]['text'];
					    }

					    if ($instance[$idbs]['title'] == 'link') {
					    	$link = $instance[$idbs]['content'];
					    }

					}
				?>
				<div class="col-lg-4">
					<div class="panel panel-default">
						<div class="panel-heading text-center">
							<h3>Baufinanzierung TOP 5</h3>
						</div>
		  				<div class="panel-body">
		  					<div class="header clearfix">
		  						<div class="anbieter pull-left">Anbieter</div>
		  						<div class="zinssatz pull-left">effektiver Jahreszins</div>
		  					</div>
		  					<div class="body">
		  						<?php if (!empty($banks)) : ?>
		  						<?php foreach ($banks as $bank) :?>
		  						<div class="item clearfix">
			  						<div class="anbieter pull-left">
			  							<strong><?php echo $bank['title']; ?></strong>
			  						</div>
			  						<div class="zinssatz pull-left">
			  							<strong><?php echo $bank['interest'];?></strong>
			  							<a href="<?php echo $bank['target'];?>" onclick="<?php echo $bank['onclick'];?>" title="zum Angebot der <?php echo $bank['title']; ?>" class="btn btn-primary btn-zinssatz"><img src="<?php echo get_template_directory_uri(); ?>/includes/img/chevron-right.png"></a>
			  						</div>
		  						</div>
			  					<?php endforeach;?>
			  					<?php endif; ?>
		  						<p><?php echo $text; ?></p>
		  						<div class="panel-body-footer text-right"><?php echo $link; ?></div>
		  					</div>
		  				</div>
		  				<div class="panel-footer"></div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="ct-testimonials">
		<div class="container">
			<div class="row">
				<?php
					$widget_content = '';
					$sidebar_id = 'sidebar-list-front';
					$sidebars_widgets = wp_get_sidebars_widgets();
					$widget_ids = $sidebars_widgets[$sidebar_id]; 
					foreach( $widget_ids as $k => $id ) {
						$wdgtvar = 'widget_'._get_widget_id_base( $id );
					    $idvar = _get_widget_id_base( $id );
					    $instance = get_option( $wdgtvar );
					    $idbs = str_replace( $idvar.'-', '', $id );
					    if (isset($instance[$idbs]['text'])) {
					    	$content_testimonials = $instance[$idbs]['text'];
					    }
					    if (isset($instance[$idbs]['title'])) {
					    	$title_testimonials = $instance[$idbs]['title'];
					    }
					}
				?>
				<h3><?php echo $title_testimonials; ?></h3>
				<?php echo $content_testimonials; ?>
			</div>
		</div>
	</section>
	<section class="ct-news">
		<div class="container">
			<div class="row">
				<div class="col-lg-6">
					<?php
					$args_latest = array(
					    'post_type' => 'post',
					    'ignore_sticky_posts' => 1,
					    'posts_per_page' => 5,
					    'category_name' => 'zinsmeldungen'
				    );
		            $the_query = new WP_Query($args_latest);
		            ?>
					<?php if (!empty($the_query->posts)) : ?>
					<div class="panel panel-default">
						<div class="panel-heading">
							<h3 class="zinsticker-front text-left">Zins-Ticker</h3>
						</div>
		  				<div class="panel-body panel-news">
		  					<div class="body">
		  						<ul class="list-unstyled">
		  							<?php while ($the_query -> have_posts()) : $the_query -> the_post(); ?>
		  							<li>
		  								<h6><?php echo date('d.m.Y', strtotime(get_the_date()));?></h6>
		  								<?php the_title( '<h3 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h3>');?>
		  							</li>
		  							<?php endwhile; ?>
		  							<?php wp_reset_query();?>
		  						</ul>
		  						<div class="panel-body-footer text-right">
		  							<a href="<?php echo get_home_url(); ?>/category/zinsmeldungen/" class="btn btn-default">Alle Meldungen <i class="fa fa-chevron-right"></i></a>
		  						</div>
		  					</div>
		  				</div>
		  				<div class="panel-footer"></div>
					</div>
					<?php endif; ?>
				</div>
				<div class="col-lg-6">
					<?php get_sidebar('homepage-ad');?>
				</div>
			</div>
			<div class="row">
				<?php
					$ids = array();
					$headline = '';
					$sidebar_id = 'aktuelle-news';
					$sidebars_widgets = wp_get_sidebars_widgets();
					$widget_ids = $sidebars_widgets[$sidebar_id]; 
					foreach( $widget_ids as $id ) {
						$wdgtvar = 'widget_'._get_widget_id_base( $id );
					    $idvar = _get_widget_id_base( $id );
					    $instance = get_option( $wdgtvar );
					    $idbs = str_replace( $idvar.'-', '', $id );
					    if (isset($instance[$idbs]['headline'])) {
					    	$headline = $instance[$idbs]['headline'];
					    }
					    $ids = array_values($instance[$idbs]);
					}
					$order_id = array_filter($ids, 'is_numeric');
				?>
				<div class="col-lg-12">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h3 class="zinsticker-front text-left"><?php echo $headline; ?></h3>
						</div>
		  				<div class="panel-body panel-news panel-aktuelle-news">
		  					<div class="body">
		  						<ul class="list-unstyled">
		  							<?php 
		  								$query = new WP_Query( array( 'post_type' => 'any', 'post__in' =>  array($order_id[1])) );
		  								while ($query -> have_posts()) : $query -> the_post(); 
		  							?>
		  							<li>
		  								<?php the_title( '<a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a>');?>
		  								<p><?php 
		  								$content = wp_strip_all_tags(get_the_content());
		  								echo wp_trim_words($content,20, '...');?></p>
		  							</li>
		  							<?php endwhile; ?>

		  							<?php 
		  								$query = new WP_Query( array( 'post_type' => 'any', 'post__in' =>  array($order_id[2])) );
		  								while ($query -> have_posts()) : $query -> the_post(); 
		  							?>
		  							<li>
		  								<?php the_title( '<a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a>');?>
		  								<p><?php 
		  								$content = wp_strip_all_tags(get_the_content());
		  								echo wp_trim_words($content,20, '...');?></p>
		  							</li>
		  							<?php endwhile; ?>

		  							<?php 
		  								$query = new WP_Query( array( 'post_type' => 'any', 'post__in' =>  array($order_id[3])) );
		  								while ($query -> have_posts()) : $query -> the_post(); 
		  							?>
		  							<li>
		  								<?php the_title( '<a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a>');?>
		  								<p><?php 
		  								$content = wp_strip_all_tags(get_the_content());
		  								echo wp_trim_words($content,20, '...');?></p>
		  							</li>
		  							<?php endwhile; ?>
		  							<?php wp_reset_query();?>
		  						</ul>
		  					</div>
		  				</div>
		  				<div class="panel-footer"></div>
					</div>
				</div>
			</div>
		</div>
	</section>

<?php
get_footer();
