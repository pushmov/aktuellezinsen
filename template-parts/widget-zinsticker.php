<?php
	$args_latest = array(
		'post_type' => 'post',
		'ignore_sticky_posts' => 1,
		'posts_per_page' => 5,
		'category_name' => 'zinsmeldungen'
	);
	$the_query = new WP_Query($args_latest); ?>
	<?php if (!empty($the_query->posts)) : ?>
	<div class="panel panel-default panel-zinsticker-widget">
		<div class="panel-heading">
			<h3><?php echo $title; ?></h3>
		</div>
		<div class="panel-body panel-news">
			<div class="body">
				<ul class="list-unstyled">
					<?php while ($the_query -> have_posts()) : $the_query -> the_post(); ?>
					<li>
						<h6><?php echo date('d.m.Y', strtotime(get_the_date()));?></h6>
						<?php the_title( '<h3 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h3>');?>
					</li>
					<?php endwhile; ?>
					<?php wp_reset_query();?>
				</ul>
				<div class="panel-body-footer text-center">
					<a href="<?php echo get_home_url(); ?>/category/zinsmeldungen/" class="btn btn-default">Alle Meldungen &gt;</a>
				</div>
			</div>
		</div>
		<div class="panel-footer"></div>
	</div>
	<?php endif; ?>