<div class="tbl-mobile clearfix">
	<?php
	$cache_key_medium = 'medium_display_t_20220119';
	if ( ! $html = get_transient( $cache_key_medium )  ) {
		$banks = $wpdb->get_results( $qry );
		ob_start();
		foreach ($banks as $bank) : ?>
	<div class="row">
		<div class="cell">
			<a href="<?php echo get_home_url().$bank->link_tagesgeld; ?>">
			<?php if ($bank->image == '' || !file_exists(get_template_directory() .'/includes/img/logos/' .  $bank->image)) : ?>
				<span class="image-txt"><?php echo str_replace('_', ' ', $bank->bank_name); ?></span>
			<?php else : ?>
				<img src="<?php echo get_template_directory_uri() .'/includes/img/logos/' .  $bank->image; ?>" title="weitere Informationen zum Tagesgeld der <?php echo str_replace('_', ' ', $bank->bank_name); ?>" class="img-responsive">
			<?php endif; ?>
			</a>
			<?php if ($bank->man_account_type != null || $bank->man_account_type != '') : ?>
				<br>
				<p class="text-left"><?php echo $bank->man_account_type; ?></p>
			<?php endif; ?>
			<h3><?php echo str_replace('_', ' ', $bank->bank_name); ?></h3>
			<ul class="list-unstyled anbieter">
				<?php if ($bank->country_name != '' ) : ?>
				<li><?php echo $bank->country_name; ?></li>
				<?php endif; ?>
				<li>Bonitat: <?php echo $bank->sANDp_rating; ?></li>
				<li><?php echo $bank->statement; ?></li>
			</ul>
		</div>
		<div class="cell">
			<?php $interestVal = $bank->man_Zinssatz_table == null || $bank->man_Zinssatz_table == '' ? $bank->Zinssatz_table : $bank->man_Zinssatz_table; ?>
			<span class="zinssatz">Zinssatz:</span><strong><?php echo number_format($interestVal, 2, ',', '.'); ?>%</strong>
			<?php if ($bank->man_Zinstermine != null || $bank->man_Zinstermine != ''): ?>
			<br>
			<p><?php echo $bank->man_Zinstermine; ?></p>
			<?php endif; ?>
		</div>
		<div class="cell">
			<h6>Bemerkungen</h6>
			<ul class="list-unstyled bemerkungen">
				<li><?php echo $bank->man_customer; ?></li>
				<?php if ($bank->zinssatz_garantie != '') : ?>
				<li><?php echo str_replace('Angebotszins gilt für die ersten', 'Zinsgarantie', $bank->zinssatz_garantie); ?></li>
				<?php else : ?>
				<li>Zinssatz variabel</li>
				<?php endif; ?>
				<li><a href="<?php echo get_home_url(); ?>/tagesgeld/<?php echo $bank->bank_name; ?>/">Produktdetails</a></li>
			</ul>
		</div>
		<div class="cell">
			<a href="<?php echo $bank->link_tagesgeld; ?>" class="btn btn-default" onclick="_gaq.push(['_trackPageview', '/vergleich/<?php echo $bank->bank_name; ?>/tagesgeld/']);return true;" title="zum Tagesgeld-Angebot der Rietumu Bank">zur Bank &gt;</a>
		</div>
		<div class="cell-detail hide">
			<ul class="list-unstyled">
				<li><span>1 Monat</span><?php echo number_format($bank->i1, 2, ',', '.'); ?>%</li>
				<li><span>3 Monate</span><?php echo number_format($bank->i3, 2, ',', '.'); ?>%</li>
				<li><span>6 Monate</span><?php echo number_format($bank->i6, 2, ',', '.'); ?>%</li>
				<li><span>9 Monate</span><?php echo number_format($bank->i9, 2, ',', '.'); ?>%</li>
			</ul>
		</div>
	</div>
	<?php endforeach;
	$html = ob_get_clean();
	set_transient( $cache_key_medium, $html, 12 * 3600 );
	}
	echo $html;
	?>
</div>
