		<div class="tbl-small clearfix">
			<?php
			$cache_key_small = 'mobile_display_t_20230527_2';
			if ( ! $html = get_transient( $cache_key_small )  ) {
				$banks = $wpdb->get_results( $qry );
				ob_start(); ?>
			<div id="accordionTagesgeldMobile" class="shadow-lg tagesgeld--small my-2">
				<?php $i = 0; ?>
				<?php foreach($banks as $key => $bank) : ?>
				<?php $i += 1; ?>
				<?php reset($banks); ?>
				<?php end($banks); ?>
				<div class="card">
					<div class="card-header" id="heading<?php echo $bank->tagesgeld_id; ?>">
						<h4 class="px-2 my-0 d-flex align-items-center justify-content-between <?php if ($i > 1): ?>border-top<?php endif; ?> ">
							<button type="button" class="btn-link px-0 py-g border-0 <?php if ($i > 1) : ?>collapsed<?php endif; ?> text-left text-truncate flex-grow-1" data-toggle="collapse" data-target="#collapse<?php echo $bank->tagesgeld_id; ?>" aria-expanded="true" aria-controls="collapse<?php echo $bank->tagesgeld_id; ?>">
								<svg class="svg--small" role="presentation" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 22.6 22.6"><path fill="#3f5fa8" d="M1.9 5.4l9.4 9.4 9.4-9.4 1.9 1.9-11.3 11.3L0 7.3l1.9-1.9z"/></svg>
								<span class="text-black fw-600"><?= ($bank->man_Zinssatz_table) ? $bank->man_Zinssatz_table : $bank->Zinssatz_table; ?>%</span>
								<span class="text-black"><?php echo str_replace('_', ' ', $bank->bank_name); ?></span>
							</button>
							<a class="d-flex align-items-center" href="<?php echo get_home_url().$bank->link_tagesgeld;?>" onclick="_gaq.push(['_trackPageview', '/vergleich/<?php echo $bank->bank_name; ?>/tagesgeld/']);return true;" title="zum Tagesgeld-Angebot der <?php echo str_replace('_', ' ', $bank->bank_name)?>">
								<svg class="svg--regular" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><g><path fill="#3f5fa8" d="M12,2.5A9.5,9.5,0,1,0,21.5,12,9.509,9.509,0,0,0,12,2.5Zm2.71,10.21-3,3a1.014,1.014,0,0,1-1.42,0,1.008,1.008,0,0,1,0-1.42L12.59,12l-2.3-2.29a1,1,0,0,1,1.42-1.42l3,3A1.008,1.008,0,0,1,14.71,12.71Z"/></g></svg>
							</a>
						</h4>
					</div>
					<div id="collapse<?php echo $bank->tagesgeld_id; ?>" class="<?php if ($i === 1 ) : ?>in<?php else : ?>collapse<?php endif; ?>" aria-labelledby="heading<?php echo $bank->tagesgeld_id; ?>" data-parent="#accordionTagesgeldMobile">
						<div class="card-body <?php if ($key === key($banks)): ?>border-top<?php else: ?>border-top<?php endif; ?> py-g bg-gray">
							<div class="cell">
								<a href="<?php echo get_home_url().$bank->link_tagesgeld; ?>">
									<?php if ($bank->image == '' || !file_exists(get_template_directory() .'/includes/img/logos/' .  $bank->image)) : ?>
										<span class="image-txt"><?php echo str_replace('_', ' ', $bank->bank_name); ?></span>
									<?php else : ?>
										<img src="<?php echo get_template_directory_uri() .'/includes/img/logos/' .  $bank->image; ?>" title="weitere Informationen zum Tagesgeld der <?php echo str_replace('_', ' ', $bank->bank_name); ?>" class="img-responsive">
									<?php endif; ?>
								</a>
								<?php if ($bank->man_account_type != null || $bank->man_account_type != '') : ?>
									<br>
									<p class="text-left"><?php echo $bank->man_account_type; ?></p>
								<?php endif; ?>
							</div>
							<div class="cell">
								<h3><?php echo str_replace('_', ' ', $bank->bank_name); ?></h3>
								<ul class="list-unstyled anbieter">
									<?php if ($bank->country_name != '' ) : ?>
									<li><?php echo $bank->country_name; ?></li>
									<?php endif; ?>
									<li>Bonitat: <?php echo $bank->sANDp_rating; ?></li>
									<li><?php echo $bank->statement; ?></li>
								</ul>
							</div>
							<div class="cell">
								<?php $interestVal = $bank->man_Zinssatz_table == null || $bank->man_Zinssatz_table == '' ? $bank->Zinssatz_table : $bank->man_Zinssatz_table; ?>
								<span class="zinssatz">Zinssatz:</span><strong><?php echo number_format($interestVal, 2, ',', '.'); ?>%</strong>
								<?php if ($bank->man_Zinstermine != null || $bank->man_Zinstermine != ''): ?>
								<br>
								<p><?php echo $bank->man_Zinstermine; ?></p>
								<?php endif; ?>
							</div>
							<div class="cell">
								<h6>Bemerkungen</h6>
								<ul class="list-unstyled bemerkungen">
									<li><?php echo $bank->man_customer; ?></li>
									<?php if ($bank->zinssatz_garantie != '') : ?>
									<li><?php echo str_replace('Angebotszins gilt für die ersten', 'Zinsgarantie', $bank->zinssatz_garantie); ?></li>
									<?php else : ?>
									<li>Zinssatz variabel</li>
									<?php endif; ?>
									<li><a href="<?php echo get_home_url(); ?>/tagesgeld/<?php echo $bank->bank_name; ?>/">Produktdetails</a></li>
								</ul>
							</div>
							<div class="cell">
								<a href="<?php echo get_home_url().$bank->link_tagesgeld;?>" class="btn btn-default" onclick="_gaq.push(['_trackPageview', '/vergleich/<?php echo $bank->bank_name; ?>/tagesgeld/']);return true;" title="zum Tagesgeld-Angebot der <?php echo str_replace('_', ' ', $bank->bank_name)?>">zur Bank &gt;</a>
							</div>
						</div>
					</div>
				</div>
				<?php endforeach; ?>
			</div>
			<?php
				$html = ob_get_clean();
				set_transient( $cache_key_small, $html, 12 * 3600 );
			}
			echo $html;
			?>
		</div>
