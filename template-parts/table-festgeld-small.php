		<div class="tbl-small clearfix">
			<?php
			$cache_key_small = 'mobile_display_20201017';
			if ( ! $html = get_transient( $cache_key_small )  ) {
				$banks = $wpdb->get_results( $qry );
				ob_start();
				foreach ($banks as $bank) : ?>
			<div class="row">
				<div class="cell">
					<a href="<?php echo get_home_url().$bank->link_festgeld; ?>">
						<?php if ($bank->image == '' || !file_exists(get_template_directory() .'/includes/img/logos/' .  $bank->image)) : ?>
							<span class="image-txt"><?php echo str_replace('_', ' ', $bank->bank_name); ?></span>
						<?php else : ?>
							<img src="<?php echo get_template_directory_uri() .'/includes/img/logos/' .  $bank->image; ?>" title="weitere Informationen zum Tagesgeld der <?php echo str_replace('_', ' ', $bank->bank_name); ?>" class="img-responsive">
						<?php endif; ?>
					</a>
					<?php if ($bank->man_account_type != null || $bank->man_account_type != '') : ?>
					<br>
					<p class="text-left"><?php echo $bank->man_account_type; ?></p>
					<?php endif; ?>
				</div>
				<div class="cell">
					<h3><?php echo str_replace('_', ' ', $bank->bank_name); ?></h3>
					<ul class="list-unstyled anbieter">
						<?php if ($bank->country_name != '' ) : ?>
						<li><?php echo $bank->country_name; ?></li>
						<?php endif; ?>
						<li>Bonitat: <?php echo $bank->sANDp_rating; ?></li>
						<li><?php echo $bank->statement; ?></li>
					</ul>
				</div>
				<div class="cell">
					<span class="zinssatz">Zinssatz:</span><strong><?php echo number_format($bank->interest, 2, ',', '.'); ?>%</strong>
					<a href="#" class="detail">Details</a>
					<div class="hide small-detail">
						<ul class="list-unstyled">
							<li><span>1 Monat</span><?php echo number_format($bank->i1, 2, ',', '.'); ?>%</li>
							<li><span>3 Monate</span><?php echo number_format($bank->i3, 2, ',', '.'); ?>%</li>
							<li><span>6 Monate</span><?php echo number_format($bank->i6, 2, ',', '.'); ?>%</li>
							<li><span>9 Monate</span><?php echo number_format($bank->i9, 2, ',', '.'); ?>%</li>
							<li><span>12 Monate</span><?php echo number_format($bank->i12, 2, ',', '.'); ?>%</li>
							<li><span>24 Monate</span><?php echo number_format($bank->i24, 2, ',', '.'); ?>%</li>
							<li><span>36 Monate</span><?php echo number_format($bank->i36, 2, ',', '.'); ?>%</li>
							<li><span>48 Monate</span><?php echo number_format($bank->i48, 2, ',', '.'); ?>%</li>
							<li><span>60 Monate</span><?php echo number_format($bank->i60, 2, ',', '.'); ?>%</li>
							<li><span>72 Monate</span><?php echo number_format($bank->i72, 2, ',', '.'); ?>%</li>
							<li><span>84 Monate</span><?php echo number_format($bank->i84, 2, ',', '.'); ?>%</li>
							<li><span>96 Monate</span><?php echo number_format($bank->i96, 2, ',', '.'); ?>%</li>
							<li><span>108 Monate</span><?php echo number_format($bank->i108, 2, ',', '.'); ?>%</li>
							<li><span>120 Monate</span><?php echo number_format($bank->i120, 2, ',', '.'); ?>%</li>
						</ul>
					</div>
				</div>
				<div class="cell">
					<h6>Bemerkungen</h6>
					<ul class="list-unstyled bemerkungen">
						<li><?php echo $bank->first_mirror_stroke; ?></li>
						<?php if ($bank->zinssatz_garantie != '') : ?>
						<li><?php echo str_replace('Angebotszins gilt für die ersten', 'Zinsgarantie', $bank->zinssatz_garantie); ?></li>
						<?php else : ?>
						<li><?php echo $bank->second_mirror_stroke; ?></li>
						<?php endif; ?>
						<li><a href="<?php echo get_home_url(); ?>/festgeld/<?php echo $bank->bank_name; ?>/">Produktdetails</a></li>
					</ul>
				</div>
				<div class="cell">
					<a href="<?php echo get_home_url().$bank->link_festgeld;?>" class="btn btn-default" onclick="_gaq.push(['_trackPageview', '/vergleich/<?php echo $bank->bank_name; ?>/festgeld/']);return true;" title="zum Tagesgeld-Angebot der <?php echo str_replace('_', ' ', $bank->bank_name)?>">zur Bank &gt;</a>
				</div>
			</div>
			<?php endforeach;
				$html = ob_get_clean();
				set_transient( $cache_key_small, $html, 12 * 3600 );
			}
			echo $html;
			?>
		</div>
