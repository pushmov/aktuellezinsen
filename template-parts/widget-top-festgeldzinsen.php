<div class="panel panel-default top-5-festgeld">
	<div class="panel-heading">
		<h3><?php echo $data['title'];?></h3>
	</div>
	<div class="panel-body panel-news">
		<div class="body">
			<ol>
				<?php foreach ($data['ct'] as $row) :?>
				<li>
			  		<a href="<?php echo get_home_url() .'/festgeld/'. $row->bank_name;?>/" title="zum Festgeld-Angebot der <?php echo $row->bank_name; ?>" onclick="_gaq.push(['_trackPageview', '/widget/<?php echo $row->bank_name; ?>/festgeld/']);return true;"><?php echo str_replace('_', ' ', $row->bank_name);?></a>
			  		<strong><?php echo number_format($row->interest, 2, ',', '.');?>%</strong>
			  	</li>
			  	<?php endforeach; ?>
			</ol>
			<div class="panel-body-footer text-right">
				<a href="<?php echo get_home_url(); ?>/festgeldzinsen/" class="btn btn-default">Alle Anbieter anzeigen</a>
			</div>
		</div>
	</div>
	<div class="panel-footer"></div>
</div>
