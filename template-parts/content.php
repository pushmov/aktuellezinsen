<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Aktuellezinsen.net
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<?php
		if ( is_singular() ) :
			the_title( '<h1 class="entry-title">', '</h1>' );
		else :
			the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
		endif;
			?>
	</header><!-- .entry-header -->

	<?php
		$custom = get_post_custom();
	?>
	<?php if (isset($custom['summary']) && current($custom['summary']) != '') : ?>
	<div class="entry-summary">
		<?php echo current($custom['summary']);?>
	</div>
	<?php endif; ?>

	<div class="entry-content">
		<?php
		the_content( sprintf(
			wp_kses(
				/* translators: %s: Name of current post. Only visible to screen readers */
				__( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'aktuellezinsen-net' ),
				array(
					'span' => array(
						'class' => array(),
					),
				)
			),
			get_the_title()
		) );

		wp_link_pages( array(
			'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'aktuellezinsen-net' ),
			'after'  => '</div>',
		) );
		?>
	</div><!-- .entry-content -->

	<?php if (isset($custom['headline_h3'])) : ?>
	<h3 class="custom-headline"><?php echo current($custom['headline_h3']);?></h3>
	<?php endif; ?>

	<?php if (isset($custom['headline_h3_content'])) : ?>
	<div class="entry-content">
		<p><?php echo current($custom['headline_h3_content']);?></p>
	</div>
	<?php endif; ?>

	<?php if (isset($custom['headline_h4'])) : ?>
	<h4 class="custom-headline"><?php echo current($custom['headline_h4']);?></h4>
	<?php endif; ?>

	<?php if (isset($custom['headline_h4_content'])) : ?>
	<div class="entry-content">
		<p><?php echo current($custom['headline_h4_content']);?>		</p>
	</div>
	<?php endif; ?>

	<div class="author clearfix">
		<?php
			$widget_content = '';
			$sidebar_id = 'sidebar-author-custom';
			$sidebars_widgets = wp_get_sidebars_widgets();
			$widget_ids = $sidebars_widgets[$sidebar_id]; 
			$profile = $url = $display_name = '';
			foreach( $widget_ids as $k => $id ) {
				$wdgtvar = 'widget_'._get_widget_id_base( $id );
				$idvar = _get_widget_id_base( $id );
				$instance = get_option( $wdgtvar );
				$idbs = str_replace( $idvar.'-', '', $id );
				if (isset($instance[$idbs]['title_author_name'])) {
					$display_name = $instance[$idbs]['title_author_name'];
				}
				if (isset($instance[$idbs]['url'])) {
				$url = $instance[$idbs]['url'];
				}

				if (isset($instance[$idbs]['text'])) {
					$profile = $instance[$idbs]['text'];
				}
			}
		?>
		<?php $author_id=$post->post_author; ?>
		<div class="col-lg-3">
			<?php if ($url == '') : ?>
			<img src="<?php echo get_template_directory_uri(); ?>/includes/img/thomas.png" width="160" class="img-responsive">
			<?php else : ?>
			<img src="<?php echo $url; ?>" width="160" class="img-responsive">
			<?php endif; ?>
		</div>
		<div class="col-lg-9">
			<p class="header"><em>Autor</em></p>
			<p class="name"><?php echo ($display_name == '') ? the_author_meta( 'display_name' , $author_id ) : $display_name; ?></p>
			<?php if ($profile == '') : ?>
			<p>hat früher bei einem Online-Broker gearbeitet. Berichtet hier über Neukunden-Aktionen, Vor- und Nachteile bestimmter Broker, Konditionsmodelle und Trading-SetUps. <a href="#">Google+</a></p>
			<?php else : ?>
			<p><?php echo $profile; ?></p>
			<?php endif; ?>
		</div>
	</div>
	
</article><!-- #post-<?php the_ID(); ?> -->
