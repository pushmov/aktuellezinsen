<?php
global $wp_query;
$name = 'Festgeld';

$interest_cols = array('1_month', '3_month', '6_month',
'9_month', '12_month', '18_month',
'24_month', '36_month', '48_month',
'60_month', '72_month', '84_month',
'96_month', '108_month', '120_month');
$interest_cols_label = array('1_month' => '1 Monat', '3_month' => '3 Monate', '6_month' => '6 Monate',
'9_month' => '9 Monate', '12_month' => '12 Monate', '18_month' => '18 Monate',
'24_month' => '24 Monate', '36_month' => '36 Monate', '48_month' => '48 Monate',
'60_month' => '60 Monate', '72_month' => '72 Monate', '84_month' => '84 Monate',
'96_month' => '96 Monate', '108_month' => '108 Monate', '120_month' => '120 Monate');

$bankname = $wp_query->query['bankname'];
$data = $wpdb->get_row( "SELECT * FROM `banks` WHERE bank_name = '{$bankname}' AND status = 'ok'" );

if (empty($data)) {
    global $wp_query;
		$wp_query->set_404();
		status_header( 404 );
		get_template_part( 404 );
		exit();
}

$data_festgeld = $wpdb->get_row("SELECT * FROM `festgeld` WHERE bank_id = '{$data->bank_id}'");
if (empty($data_festgeld)) {
    global $wp_query;
		$wp_query->set_404();
		status_header( 404 );
		get_template_part( 404 );
		exit();
}

$highest_interest_rate = $wpdb->get_row("SELECT GREATEST(".join(',', $interest_cols).") AS greatest FROM `festgeld` where bank_id = {$data->bank_id}");
$array_interest = (array) $data_festgeld;

if (empty($data_festgeld)) {
  $data_festgeld = new StdClass();
  $data_festgeld->festgeld_id = 0;
  $data_festgeld->bank_id = 0;
  $data_festgeld->product_name = 0;
  $data_festgeld->Zinssatz_table = 0;
  $data_festgeld->anlagebetrag_minimum = 0;
  $data_festgeld->anlagebetrag_maximum = 0;
  $data_festgeld->Zinstermine = 0;
  $data_festgeld->besonderheit = 0;
  $data_festgeld->verfügbar_fuer = 0;
  $data_festgeld->orginaldatensatz_von = 0;

  $data_festgeld->zinssatz_garantie = 0;
  $data_festgeld->month_1 = 0;
  $data_festgeld->month_3 = 0;
  $data_festgeld->month_6 = 0;
  $data_festgeld->month_9 = 0;

  $data_festgeld->month_12 = 0;
  $data_festgeld->month_18 = 0;
  $data_festgeld->month_24 = 0;
  $data_festgeld->month_36 = 0;

  $data_festgeld->month_48 = 0;
  $data_festgeld->month_60 = 0;
  $data_festgeld->month_72 = 0;
  $data_festgeld->month_84 = 0;

  $data_festgeld->month_96 = 0;
  $data_festgeld->month_108 = 0;
  $data_festgeld->month_120 = 0;
  $data_festgeld->created = 0;
  $data_festgeld->updated = 0;

  $array_interest = array(
    'month_1' => 0,
    'month_3' => 0,
    'month_6' => 0,
    'month_9' => 0,
    'month_12' => 0,

    'month_18' => 0,
    'month_24' => 0,
    'month_36' => 0,
    'month_48' => 0,
    'month_60' => 0,

    'month_72' => 0,
    'month_84' => 0,
    'month_96' => 0,
    'month_108' => 0,
    'month_120' => 0
  );

  $highest_interest_rate = new StdClass();
  $highest_interest_rate->greatest = 0;
}

function remove_zero($var) {
  return !((float)$var === 0.0);
}
$filters = array_filter($array_interest, 'remove_zero');
foreach ($filters as $k => $row) {
  if (!in_array($k, $interest_cols)) {
    unset($filters[$k]);
  }
}

get_header('festgeld-detail');
?>

<section class="ct-news ct-festgeld ct-festgeld-detail">
  <div class="container">
    <div class="row">
      <div class="col-sm-12 col-md-9">
        <h1><?php echo $name . ' ' . str_replace('_', ' ', $data->bank_name);?></h1>
        <ul class="breadcrumb" itemscope itemtype="http://schema.org/BreadcrumbList">
          <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
            <a itemscope itemtype="http://schema.org/Thing" itemprop="item" href="<?php echo get_home_url(); ?>/festgeldzinsen/">
              <span itemprop="name">Festgeldzinsen Vergleich</span>
            </a>
            <meta itemprop="position" content="1" />
          </li>
          <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
            <a itemscope itemtype="http://schema.org/Thing" itemprop="item" href="<?php echo get_home_url(); ?>/festgeld/<?php echo $data->bank_name; ?>/">
              <span itemprop="name"><?php echo str_replace('_', ' ', $data->bank_name);?></span>
            </a>
            <meta itemprop="position" content="2" />
          </li>
        </ul>
        <div class="summary">
          <ul class="list-unstyled">
            <li>Bis zu <strong><?php echo number_format($highest_interest_rate->greatest, 2, ',', '.'); ?>%</strong> p.a. Festgeldzinsen</li>
            <li><?php echo $data->Einlagensicherung_detail; ?></li>
            <?php if ($data_festgeld->anlagebetrag_maximum != '0') : ?>
            <li>Anlagebetrag bis zu <?php echo number_format($data_festgeld->anlagebetrag_maximum, 0, ',', '.');?> &euro;</li>
            <?php endif; ?>
          </ul>
          <p><?php echo $data->ueber_bank; ?></p>
        </div>

        <div class="info-box">
          <h3>Angebotsdetails</h3>
          <table class="table" width="100%" border="0">
            <tr>
              <td>Anlagebetrag Minimum:</td>
              <td><?php echo number_format($data_festgeld->anlagebetrag_minimum, 0, ',', '.');?> &euro;</td>
            </tr>
            <tr>
              <td>Anlagebetrag Maximum:</td>
              <td><?php echo ($data_festgeld->anlagebetrag_maximum == '0' ? 'n.a.' : number_format($data_festgeld->anlagebetrag_maximum, 0, ',', '.').' &euro;')?></td>
            </tr>
            <tr>
              <td>Zinstermine:</td>
              <td><?php echo ($data_festgeld->Zinstermine == '' ? 'n.a.' : $data_festgeld->Zinstermine); ?></td>
            </tr>
            <?php if ($data_festgeld->besonderheit != '') : ?>
            <tr>
              <td>Bemerkungen:</td>
              <td><?php echo $data_festgeld->besonderheit; ?></td>
            </tr>
            <?php endif; ?>
            <tr>
              <td>Einlagensicherung:</td>
              <td><?php echo $data->Einlagensicherung; ?></td>
            </tr>
          </table>
        </div>

        <h3 class="description-title">Einlagensicherung <?php echo $data->country_name; ?></h3>
        <div class="summary description">
        <p><?php echo $data->Einlagensicherung_description; ?></p>
        </div>
      </div>
      <div class="col-sm-12 col-md-3">
        <div class="bank-info">
          <?php if ($data->image == '' || !file_exists(get_template_directory() .'/includes/img/logos/' .  $data->image)) : ?>
            <span class="image-txt"><?php echo str_replace('_', ' ', $data->bank_name); ?></span>
          <?php else : ?>
            <img src="<?php echo get_template_directory_uri() .'/includes/img/logos/' .  $data->image; ?>" title="<?php echo str_replace('_', ' ', $data->bank_name); ?>" class="img-responsive">
          <?php endif; ?>
          <ul class="list-unstyled">
            <li>aus <?php echo $data->country_name; ?></li>
            <li>Bonitat: <?php echo $data->sANDp_rating; ?></li>
            <?php if ($data->kontakt != '') : ?>
            <li>Tel: <?php echo $data->kontakt; ?></li>
            <?php endif; ?>
          </ul>
          <a href="<?php echo get_home_url().str_replace(' ', '', $data->link_festgeld); ?>" class="btn btn-default" onclick="_gaq.push(['_trackPageview', '/detailseite/<?php echo $data->bank_name; ?>/festgeld/']);return true;">zur Bank ></a>
        </div>

        <?php if (!empty($filters)) : ?>
        <div class="panel panel-default top-5-festgeld zinssatz">
          <div class="panel-heading">
            <h3>Zinssätze</h3>
          </div>
          <div class="panel-body panel-news">
            <div class="body">
              <ul class="list-unstyled">
                <?php foreach ($filters as $id => $val) : ?>
                <li>
                  <div class="clearfix">
                    <span><?php echo $interest_cols_label[$id]; ?></span><strong class="pull-right"><?php echo number_format($val, 2, ',', '.');?>%</strong>
                  </div>
                </li>
                <?php endforeach; ?>
              </ul>
              <div class="panel-body-footer text-right">
                <a href="<?php echo get_home_url().str_replace(' ', '', $data->link_festgeld); ?>" class="btn btn-default" onclick="_gaq.push(['_trackPageview', '/detailseite/<?php echo $data->bank_name; ?>/festgeld/']);return true;">jetzt anlegen ></a>
              </div>
            </div>
          </div>
          <div class="panel-footer"></div>
        </div>
        <?php endif; ?>
      </div>
    </div>
  </div>
</section>
<?php get_footer(); ?>
