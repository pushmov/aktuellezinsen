<?php

get_header();

?>
<section class="ct-news ct-category">
		<div class="container">
			<div class="row">
				<div class="col-md-9">
					<?php 	
						$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
			            $args_latest = array(           
				            'post_type' => 'post',
				            'ignore_sticky_posts' => 1,
				            'posts_per_page' => 15,
				            'paged' => $paged
			        	);
	            		$the_query = new WP_Query($args_latest); ?>
			            <ul class="list-unstyled list-category-news">
			  							<?php while ($the_query -> have_posts()) : $the_query -> the_post(); ?>
			  							<li>
			  								<?php the_title( '<h3><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h3>');?>
			  								<p><?php 
			  								$content = wp_strip_all_tags(get_the_content());
			  								echo wp_trim_words($content,50, '...');?></p>
			  							</li>
			  							<?php endwhile; ?>
			  							<?php wp_reset_query();?>
			  						</ul>

						<div class="pull-left">
							<?php next_posts_link( '&larr; Older posts', $wp_query ->max_num_pages); ?>
						</div>

						<div class="pull-right">
							<?php previous_posts_link( 'Newer posts &rarr;' ); ?>
						</div>
						
						
				</div>
				<div class="col-md-3">
					<?php get_sidebar('news'); ?>
				</div>
			</div>
		</div>
</section>
<?php get_footer(); ?>