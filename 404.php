<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package Aktuellezinsen.net
 */

get_header();
?>
<div id="primary" class="content-area">
	<section class="error-404 not-found">
		<div class="container">
			<div class="row">
				<header class="page-header">
					<h1 class="page-title"><?php esc_html_e( 'Oops! That page can&rsquo;t be found.', 'aktuellezinsen-net' ); ?></h1>
				</header><!-- .page-header -->
				<div class="page-content">
					<div class="col-lg-9">
						<p><?php esc_html_e( 'It looks like nothing was found at this location. Maybe try one of the links below or a search?', 'aktuellezinsen-net' ); ?></p>

						<?php
						get_search_form();

						the_widget( 'WP_Widget_Recent_Posts' );
						?>
					</div>
					<div class="col-lg-3">
						<div class="widget widget_categories">
							<h2 class="widget-title">
								<?php esc_html_e( 'Most Used Categories', 'aktuellezinsen-net' ); ?>
							</h2>
							<ul>
								<?php
								wp_list_categories( array(
									'orderby'    => 'count',
									'order'      => 'DESC',
									'show_count' => 1,
									'title_li'   => '',
									'number'     => 10,
								) );
								?>
							</ul>
						</div><!-- .widget -->
						<div>
							<?php
						/* translators: %1$s: smiley */
						$aktuellezinsen_net_archive_content = '<p>' . sprintf( esc_html__( 'Try looking in the monthly archives. %1$s', 'aktuellezinsen-net' ), convert_smilies( ':)' ) ) . '</p>';
						the_widget( 'WP_Widget_Archives', 'dropdown=1', "after_title=</h2>$aktuellezinsen_net_archive_content" );

						
						?>
						</div>
						<div>
							<?php the_widget( 'WP_Widget_Tag_Cloud' ); ?>
						</div>
					</div>
					

					

					

				</div><!-- .page-content -->
			</div>
		</div>
	</section>	
</div><!-- #primary -->

<?php
get_footer();
