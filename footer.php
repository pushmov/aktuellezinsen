<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Aktuellezinsen.net
 */

?>

	</div><!-- #content -->

	<footer>
		<div class="container">
			<div class="text-center">
				<?php
					wp_nav_menu( array(
						'theme_location' => 'menu-1',
						'menu'        => 'new-footer',
						'menu_class' => 'list-unstyled footer-menu'
					));
				?>
				<span>&copy; 2019 - 2023 Aktuellezinsen.net</span>
			</div>
			<?php
				$widget_content = '';
				$sidebar_id = 'footer-1';
				$sidebars_widgets = wp_get_sidebars_widgets();
				$widget_ids = $sidebars_widgets[$sidebar_id]; 
				foreach( $widget_ids as $id ) {
					$wdgtvar = 'widget_'._get_widget_id_base( $id );
				    $idvar = _get_widget_id_base( $id );
				    $instance = get_option( $wdgtvar );
				    $idbs = str_replace( $idvar.'-', '', $id );
				    $widget_content = $instance[$idbs]['content'];
				}
			?>
			<?php echo $widget_content; ?>
		</div>
	</footer>
</div><!-- #page -->

<?php wp_footer(); ?>
</body>
</html>
