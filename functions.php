<?php
/**
 * Aktuellezinsen.net functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Aktuellezinsen.net
 */

if ( ! function_exists( 'aktuellezinsen_net_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function aktuellezinsen_net_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on Aktuellezinsen.net, use a find and replace
		 * to change 'aktuellezinsen-net' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'aktuellezinsen-net', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'aktuellezinsen-net' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'aktuellezinsen_net_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'aktuellezinsen_net_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function aktuellezinsen_net_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'aktuellezinsen_net_content_width', 640 );
}
add_action( 'after_setup_theme', 'aktuellezinsen_net_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function aktuellezinsen_net_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'aktuellezinsen-net' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'aktuellezinsen-net' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'Footer', 'aktuellezinsen-net' ),
		'id'            => 'footer-1',
		'description'   => esc_html__( 'Add widgets here.', 'aktuellezinsen-net' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'Baufinanzierung Table', 'aktuellezinsen-net' ),
		'id'            => 'custom-top-5',
		'description'   => esc_html__( 'Add widgets here.', 'aktuellezinsen-net' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'Aktuelle News', 'aktuellezinsen-net' ),
		'id'            => 'aktuelle-news',
		'description'   => esc_html__( 'Add widgets here.', 'aktuellezinsen-net' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'Tagesgeldzinsen Page - Free Text', 'aktuellezinsen-net' ),
		'id'            => 'wissenswertes',
		'description'   => esc_html__( 'Add widgets here.', 'aktuellezinsen-net' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'Festgeldzinsen Page - Free Text', 'aktuellezinsen-net' ),
		'id'            => 'wissenswertes-f',
		'description'   => esc_html__( 'Add widgets here.', 'aktuellezinsen-net' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'Tagesgeldzinsen Page - Top 5', 'aktuellezinsen-net' ),
		'id'            => 'sidebar-tagesgeld',
		'description'   => esc_html__( 'Add widgets here.', 'aktuellezinsen-net' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'Festgeldzinsen Page - Top 5', 'aktuellezinsen-net' ),
		'id'            => 'sidebar-festgeld',
		'description'   => esc_html__( 'Add widgets here.', 'aktuellezinsen-net' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'News Detail Sidebar', 'aktuellezinsen-net' ),
		'id'            => 'sidebar-news',
		'description'   => esc_html__( 'Add widgets here.', 'aktuellezinsen-net' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'Homepage Ad', 'aktuellezinsen-net' ),
		'id'            => 'sidebar-homepage-ad',
		'description'   => esc_html__( 'Add widgets here.', 'aktuellezinsen-net' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Front Page List', 'aktuellezinsen-net' ),
		'id'            => 'sidebar-list-front',
		'description'   => esc_html__( 'Add widgets here.', 'aktuellezinsen-net' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'Custom Author', 'aktuellezinsen-net' ),
		'id'            => 'sidebar-author-custom',
		'description'   => esc_html__( 'Add widgets here.', 'aktuellezinsen-net' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'aktuellezinsen_net_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function aktuellezinsen_net_scripts() {
	wp_enqueue_style( 'aktuellezinsen-net-style', get_stylesheet_uri() );

	wp_enqueue_script( 'aktuellezinsen-net-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );

	wp_enqueue_script( 'aktuellezinsen-net-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );
	wp_enqueue_script( 'aktuellezinsen-net-bootstrap-js', get_template_directory_uri() . '/includes/js/bootstrap.min.js', array(), '20151215', true );
	wp_enqueue_script( 'aktuellezinsen-net-bootstrap-select-js', get_template_directory_uri() . '/includes/js/bootstrap-select.min.js', array(), '20151215', true );
	wp_enqueue_script( 'aktuellezinsen-net-custom-js', get_template_directory_uri() . '/includes/js/custom.js', array(), '20151215', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'aktuellezinsen_net_scripts' );


if ( ! function_exists( 'recursive_mitems_to_array' ) ) {
    /**
     * @param $items
     * @param int $parent
     *
     * @return array
     */
    function recursive_mitems_to_array( $items, $parent = 0 )
    {
        $bundle = [];
        foreach ( $items as $item ) {
            if ( $item->menu_item_parent == $parent ) {
                $child               = recursive_mitems_to_array( $items, $item->ID );
                $bundle[ $item->ID ] = [
                    'item'   => $item,
                    'childs' => $child
                ];
            }
        }

        return $bundle;
    }
}

function custom_rewrite_rule() {
    $festgeld = get_page_by_path('detailpages-for-festgeld-offers');
		$tagesgeld = get_page_by_path('detailpages-for-tagesgeld-offers');
    add_rewrite_rule('^tagesgeld/([^/]*)/?','index.php?page_id='.$tagesgeld->ID.'&offer=tagesgeld&bankname=$matches[1]','top');
		add_rewrite_rule('^festgeld/([^/]*)/?','index.php?page_id='.$festgeld->ID.'&offer=festgeld&bankname=$matches[1]','top');
}
add_action('init', 'custom_rewrite_rule', 10, 0);

function custom_rewrite_tag() {
  add_rewrite_tag('%bankname%', '([^&]+)');
	add_rewrite_tag('%offer%', '([^&]+)');
}
add_action('init', 'custom_rewrite_tag', 10, 0);

function get_myname() {
    return 'My name is Moses';
}

function get_bank_name() {
	global $wp_query;
	return str_replace('_', ' ', $wp_query->query['bankname']);
}

function get_highest_interest() {
	global $wp_query;
	global $wpdb;
	$bankname = $wp_query->query['bankname'];
	$data = $wpdb->get_row( "SELECT * FROM `banks` WHERE bank_name = '{$bankname}' AND status = 'ok'" );
	if ($wp_query->query['offer'] == 'festgeld') {
		$interest_cols = array('1_month', '3_month', '6_month',
		'9_month', '12_month', '18_month',
		'24_month', '36_month', '48_month',
		'60_month', '72_month', '84_month',
		'96_month', '108_month', '120_month');
		$interest = $wpdb->get_row("SELECT GREATEST(".join(',', $interest_cols).") AS greatest FROM `festgeld` where bank_id = {$data->bank_id}");
		$highest_interest_rate = $interest->greatest;
	} else {
		$data_tagesgeld = $wpdb->get_row("SELECT * FROM `tagesgeld` WHERE bank_id = '{$data->bank_id}'");
		$highest_interest_rate = $data_tagesgeld->Zinssatz_table;
	}
	return number_format($highest_interest_rate, 2, ',', '.').'%';
}

function get_country_name() {
	global $wp_query;
	global $wpdb;
	$bankname = $wp_query->query['bankname'];
	$data = $wpdb->get_row( "SELECT * FROM `banks` WHERE bank_name = '{$bankname}' AND status = 'ok'" );
	return $data->country_name;
}

function get_anlagebetrag_maximum() {
	global $wp_query;
	global $wpdb;
	$bankname = $wp_query->query['bankname'];
	$data = $wpdb->get_row( "SELECT * FROM `banks` WHERE bank_name = '{$bankname}' AND status = 'ok'" );
	if ($wp_query->query['offer'] == 'festgeld') {
		$festgeld = $wpdb->get_row("SELECT * FROM `festgeld` WHERE bank_id = '{$data->bank_id}'");
		return ($festgeld->anlagebetrag_maximum != '') ? number_format($festgeld->anlagebetrag_maximum, 0, ',', '.').' &euro;' : 0;
	} else {
		$tagesgeld = $wpdb->get_row("SELECT * FROM `tagesgeld` WHERE bank_id = '{$data->bank_id}'");
		return ($tagesgeld->anlagebetrag_maximum != '') ? number_format($tagesgeld->anlagebetrag_maximum, 0, ',', '.').' &euro;' : 0;
	}
}

function register_custom_yoast_variables() {
    wpseo_register_var_replacement( '%%myname%%', 'get_myname', 'advanced', 'some help text' );
		wpseo_register_var_replacement( '%%bank_name%%', 'get_bank_name', 'advanced', 'some help text' );
		wpseo_register_var_replacement( '%%highest_interest%%', 'get_highest_interest', 'advanced', 'some help text' );
		wpseo_register_var_replacement( '%%country_name%%', 'get_country_name', 'advanced', 'some help text' );
		wpseo_register_var_replacement( '%%anlagebetrag_maximum%%', 'get_anlagebetrag_maximum', 'advanced', 'some help text' );
}
add_action('wpseo_register_extra_replacements', 'register_custom_yoast_variables');
add_filter('wpseo_title', 'filter_page_wpseo_title');
function filter_page_wpseo_title($title) {
	return $title;
}
add_filter( 'wpseo_canonical', '__return_false' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

// Register and load the widget
function baufinanzierung_load_widget() {
    register_widget( 'wpb_widget' );
    register_widget( 'aktuellenews_widget' );
    register_widget( 'topfestgeld_widget' );
    register_widget( 'toptagesgeld_widget' );
    register_widget( 'zinsticker_widget' );
    register_widget( 'author_widget' );
}
add_action( 'widgets_init', 'baufinanzierung_load_widget' );

function add_custom_query_var( $vars ){
  $vars[] = 'o';
  return $vars;
}
add_filter( 'query_vars', 'add_custom_query_var' );

require_once( get_template_directory() . '/classes/wpb_widget.php' );
require_once( get_template_directory() . '/classes/aktuellenews_widget.php' );
require_once( get_template_directory() . '/classes/topfestgeld_widget.php' );
require_once( get_template_directory() . '/classes/toptagesgeld_widget.php' );
require_once( get_template_directory() . '/classes/zinsticker_widget.php' );
require_once( get_template_directory() . '/classes/author_widget.php' );
