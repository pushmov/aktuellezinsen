<?php get_header(); ?>

<section class="ct-news ct-tagesgeld">
	<div class="container">
		<h1>Tagesgeld</h1>
		<div class="row">
			<div class="col-lg-12">
				<div class="divTable table-tagesgeld tbl-desktop">
					<div class="divTableBody">
						<div class="divTableRow header">
							<div class="divTableCell">Anbieter</div>
							<div class="divTableCell">Zinssatz</div>
							<div class="divTableCell">Bemerkungen</div>
							<div class="divTableCell">Zum Anbieter</div>
						</div>
						<?php
							$cache_key = 'my-expensive-query-tagesgeld-20220119';
							$qry = "SELECT tagesgeld.tagesgeld_id, banks.image,banks.link_tagesgeld,banks.bank_name,banks.country_name,banks.sANDp_rating,tagesgeld.man_Zinssatz_table,tagesgeld.Zinssatz_table, tagesgeld.ranking_table,banks.statement, tagesgeld.zinssatz_garantie, tagesgeld.man_Zinstermine, tagesgeld.man_account_type, tagesgeld.man_customer FROM `tagesgeld` INNER JOIN banks ON banks.bank_id = tagesgeld.bank_id WHERE banks.tagesgeld_id IS NOT NULL AND banks.status = 'ok' AND Zinssatz_table > 0 ORDER BY ranking_table DESC ";

							#echo $qry;

							if ( ! $html = get_transient( $cache_key ) ) {
							    $banks = $wpdb->get_results( $qry );
							    ob_start();
							    foreach ($banks as $bank) : ?>
								<div class="divTableRow body">
									<div class="divTableCell cellBorder divTableAnbieter">
										<div class="clearfix">
											<div class="col-lg-6">
												<a href="<?php echo get_home_url().str_replace(' ', '', $bank->link_tagesgeld); ?>">
												<?php if ($bank->image == '' || !file_exists(get_template_directory() .'/includes/img/logos/' .  $bank->image)) : ?>
													<span class="image-txt"><?php echo str_replace('_', ' ', $bank->bank_name); ?></span>
												<?php else : ?>
													<img src="<?php echo get_template_directory_uri() .'/includes/img/logos/' .  $bank->image; ?>" title="weitere Informationen zum Tagesgeld der <?php echo str_replace('_', ' ', $bank->bank_name); ?>" class="img-responsive">
												<?php endif; ?>
												</a>
												<?php if ($bank->man_account_type != null || $bank->man_account_type != '') : ?>
												<br>
												<p class="text-left"><?php echo $bank->man_account_type; ?></p>
												<?php endif; ?>
											</div>
											<div class="col-lg-6">
												<h3><?php echo str_replace('_', ' ', $bank->bank_name); ?></h3>
												<ul class="list-unstyled anbieter">
													<?php if ($bank->country_name != '' ) : ?>
													<li><?php echo $bank->country_name; ?></li>
													<?php endif; ?>
													<li>Bonitat: <?php echo $bank->sANDp_rating; ?></li>
													<li><?php echo $bank->statement; ?></li>
												</ul>
											</div>
										</div>
									</div>
									<div class="divTableCell cellBorder zinssatz-row">
										<?php $interestVal = $bank->man_Zinssatz_table == null || $bank->man_Zinssatz_table == '' ? $bank->Zinssatz_table : $bank->man_Zinssatz_table; ?>
										<span>Zinssatz:</span><strong><?php echo number_format($interestVal, 2, ',', '.'); ?>%</strong>
										<?php if ($bank->man_Zinstermine != null || $bank->man_Zinstermine != ''): ?>
										<br>
										<p><?php echo $bank->man_Zinstermine; ?></p>
										<?php endif; ?>
									</div>
									<div class="divTableCell cellBorder bemerkungen-cell">
										<ul class="list-unstyled bemerkungen">
											<li><?php echo $bank->man_customer; ?></li>
											<?php if ($bank->zinssatz_garantie != '') : ?>
											<li><?php echo str_replace('Angebotszins gilt für die ersten', 'Zinsgarantie', $bank->zinssatz_garantie); ?></li>
											<?php else : ?>
											<li>Zinssatz variabel</li>
											<?php endif; ?>
											<li><a href="<?php echo get_home_url(); ?>/tagesgeld/<?php echo $bank->bank_name; ?>/">Produktdetails</a></li>
										</ul>
									</div>
									<div class="divTableCell cellBorder">
										<a href="<?php echo get_home_url() . str_replace(' ', '', $bank->link_tagesgeld); ?>" class="btn btn-default" onclick="_gaq.push(['_trackPageview', '/vergleich/<?php echo $bank->bank_name; ?>/tagesgeld/']);return true;" title="zum Tagesgeld-Angebot der <?php echo str_replace('_', ' ', $bank->bank_name); ?>">zur Bank &gt;</a>
									</div>
								</div>
								<?php endforeach;
							    $html = ob_get_clean();
							    set_transient( $cache_key, $html, 12 * 3600 );
							}

							echo $html;
						?>

					</div>
				</div>

				<?php include(get_template_directory() . '/template-parts/table-tagesgeld-medium.php');?>
				<?php include(get_template_directory() . '/template-parts/table-tagesgeld-small.php'); ?>

				<div class="tagesgeld-footer-date text-right">
					<p>Zuletzt aktualisiert: <?php echo date('d. m. Y');?></p>
				</div>

			</div>
		</div>



		<div class="row">
			<div class="col-lg-9">
				<?php
				$args_latest = array(
					'post_type' => 'post',
					'ignore_sticky_posts' => 1,
					'posts_per_page' => 5,
					'category_name' => 'zinsmeldungen'
				);
		        $the_query = new WP_Query($args_latest);
		        ?>
				<?php if (!empty($the_query->posts)) : ?>
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="zinsticker-front text-left">Zins-Ticker</h3>
					</div>
		  			<div class="panel-body panel-news">
		  				<div class="body">
		  					<ul class="list-unstyled">
		  						<?php while ($the_query -> have_posts()) : $the_query -> the_post(); ?>
		  						<li>
		  							<h6><?php echo date('d.m.Y', strtotime(get_the_date()));?></h6>
		  							<?php the_title( '<h3 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h3>');?>
		  						</li>
		  						<?php endwhile; ?>
		  						<?php wp_reset_query();?>
		  					</ul>
		  					<div class="panel-body-footer text-right">
		  						<a href="<?php echo get_home_url(); ?>/category/zinsmeldungen/" class="btn btn-default">Alle Meldungen <i class="fa fa-chevron-right"></i></a>
		  					</div>
		  				</div>
		  			</div>
		  			<div class="panel-footer"></div>
				</div>
				<?php endif; ?>

				<?php
					$widget_content = '';
					$sidebar_id = 'wissenswertes';
					$sidebars_widgets = wp_get_sidebars_widgets();
					$widget_ids = $sidebars_widgets[$sidebar_id];
					foreach( $widget_ids as $k => $id ) {
						$wdgtvar = 'widget_'._get_widget_id_base( $id );
					    $idvar = _get_widget_id_base( $id );
					    $instance = get_option( $wdgtvar );
					    $idbs = str_replace( $idvar.'-', '', $id );
					    if (isset($instance[$idbs]['text'])) {
					    	$widget_content[$k] = $instance[$idbs]['text'];
					    }
					    if (isset($instance[$idbs]['title'])) {
					    	$widget_title[$k] = $instance[$idbs]['title'];
					    }
					}
				?>
				<div class="panel panel-default wissenswertes">

					<div class="panel-heading">
						<h3 class="zinsticker-front text-left"><?php echo $widget_title[0]; ?></h3>
					</div>
			  		<div class="panel-body panel-news">
			  			<div class="body">
			  				<?php echo $widget_content[0]; ?>
			  			</div>
			  		</div>
			  		<div class="panel-footer"></div>
				</div>
			</div>
			<div class="col-lg-3">
				<?php get_sidebar('tagesgeldzinsen');?>
			</div>
		</div>
	</div>
</section>

<?php get_footer(); ?>
