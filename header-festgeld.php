<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Aktuellezinsen.net
 */
global $wpdb;
$cache_tagesgelt = 'tagesgelt_query';
$cache_festgelt = 'festgelt_query';

if ( ! $htmlt = get_transient( $cache_tagesgelt )  ) {
	$tagesgelt = $wpdb->get_row( "SELECT MAX(Zinssatz_table) AS interest FROM `tagesgeld` INNER JOIN banks ON banks.bank_id = tagesgeld.bank_id WHERE banks.status = 'ok' LIMIT 0,1 " );
	$htmlt = '<strong>' . $tagesgelt->interest . '%</strong>';
	set_transient( $cache_tagesgelt, $htmlt, 12 * 3600 );
}

if ( ! $htmlf = get_transient( $cache_festgelt )  ) {
	$festgelt = $wpdb->get_row( "SELECT MAX(48_month) AS interest FROM `festgeld` INNER JOIN banks ON banks.bank_id = festgeld.bank_id WHERE banks.status = 'ok' LIMIT 0,1 " );
	$htmlf = '<strong>' . $festgelt->interest . '%</strong>';
	set_transient( $cache_festgelt, $htmlf, 12 * 3600 );
}
global $wp_query;
?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700" rel="stylesheet"> 
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/includes/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/includes/css/bootstrap-select.min.css">

	<?php wp_head(); ?>

	<?php if (isset($wp_query->query['o'])) : ?>
	<meta name="robots" content="noindex,follow">
	<?php else : ?>
	<meta name="robots" content="index,follow">
	<?php endif; ?>
	
	<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-TMFHX3K');</script>
<!-- End Google Tag Manager -->
</head>

<body <?php body_class(); ?>>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TMFHX3K"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
	
<div id="page" class="site">
	<header id="masthead" class="site-header">
		<nav class="navbar navbar-default">
			<div class="container-fluid maxwidth-1">
				<div class="navbar-header">
		      		<a class="navbar-brand" href="<?php echo get_home_url(); ?>"><img src="<?php echo get_template_directory_uri(); ?>/includes/img/Aktuellezinsen-net-Logo-weiss-small.png" height="35"></a>
		      		<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#toggle-menu" aria-expanded="false">
        				<span class="sr-only">Toggle navigation</span>
        				<span class="icon-bar"></span>
        				<span class="icon-bar"></span>
        				<span class="icon-bar"></span>
      				</button>
		    	</div>
		    	<div class="collapse navbar-collapse" id="toggle-menu">
		    		<div class="text-center">
		    			<?php
				        	
							$menu_locations = wp_get_nav_menu_items('topnav');
							$build_tree = recursive_mitems_to_array( $menu_locations );
							
				        ?>
				        <ul class="nav navbar-nav">
				        	<?php foreach ($build_tree as $item) : ?>
				        	<li <?php echo (!empty($item['childs'])) ? ' class="dropdown"' : ''; ?>>
				        		<?php if (!empty($item['childs'])) : ?>
				        		<a href="<?php echo $item['item']->url; ?>" <?php echo $item['item']->attr_title; ?>><?php echo $item['item']->post_title; ?><i class="fa fa-chevron-down"></i></a>
				        			<ul class="dropdown-menu sub-menu">
				        				<?php foreach ($item['childs'] as $child) : ?>
						                <li <?php echo (!empty($child['childs'])) ? ' class="dropdown dropdown-submenu"' : ''; ?>>
						                	<a href="<?php echo $child['item']->url; ?>" title="<?php echo $child['item']->attr_title; ?>"><?php echo $child['item']->post_title; ?></a>
						                	<?php if (!empty($child['childs'])) : ?>
						                	<ul class="dropdown-menu sub-menu">
						                		<?php foreach ($child['childs'] as $child2) : ?>
						                        <li><a href="<?php echo $child2['item']->url; ?>" title="<?php echo $child2['item']->attr_title; ?>"><?php echo $child2['item']->post_title; ?></a></li>
						                    	<?php endforeach; ?>
						                    </ul>
						                <?php endif; ?>
						                </li>
						            	<?php endforeach; ?>
						            </ul>
				        		<?php else : ?>
				        		<a href="<?php echo $item['item']->url; ?>" title="<?php echo $item['item']->attr_title; ?>"><?php echo $item['item']->post_title; ?> <?php 
				        			if ($item['item']->post_title == 'Tagesgeld') {
				        				echo $htmlt;
				        			} elseif($item['item']->post_title == 'Festgeld') {
				        				echo $htmlf;
				        			}
				        		?></a>
				        		<?php endif; ?>
				        	</li>
				        	<?php endforeach; ?>
				        </ul>
			    	</div>
			    </div>
			</div>
			
			<button class="menu-toggle hide" aria-controls="primary-menu" aria-expanded="false"><?php esc_html_e( 'Primary Menu', 'aktuellezinsen-net' ); ?></button>

		</nav><!-- #site-navigation -->
	</header><!-- #masthead -->

	<div id="content" class="site-content">
