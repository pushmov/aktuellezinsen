<div class="adx-widget">
<?php
	$widget_content = '';
	$sidebar_id = 'sidebar-homepage-ad';
	$sidebars_widgets = wp_get_sidebars_widgets();
	$widget_ids = $sidebars_widgets[$sidebar_id]; 
	foreach( $widget_ids as $id ) {
		$wdgtvar = 'widget_'._get_widget_id_base( $id );
		$idvar = _get_widget_id_base( $id );
		$instance = get_option( $wdgtvar );
		$idbs = str_replace( $idvar.'-', '', $id );
		$widget_content = $instance[$idbs]['content'];
		$widget_title = $instance[$idbs]['title'];
	}
	echo $widget_content;
?>
</div>