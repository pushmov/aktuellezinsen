<?php get_header('festgeld');
$o = (isset($wp_query->query['o'])) ? $wp_query->query['o'] : 48;
?>
<section class="ct-news ct-festgeld">
	<div class="container">
		<div class="row festgeld-w">
			<div class="col-sm-4 col-md-4">
				<h1>Festgeld</h1>
			</div>
			<div class="col-sm-8 col-md-8 text-right">
				<form class="form-horizontal form-laufzeit">
					<div class="form-group">
						<label for="Monat" class="col-sm-6 col-md-6 control-label text-center">Laufzeit:</label>
						<div class="col-sm-6 col-md-6">
							<select id="Monat" class="selectpicker" data-href="<?php echo get_home_url(); ?>/festgeldzinsen/">
								<option value="1" <?php echo ($o == 1) ? 'selected="selected"' : ''; ?>>1 Monat</option>
								<option value="3" <?php echo ($o == 3) ? 'selected="selected"' : ''; ?>>3 Monate</option>
								<option value="6" <?php echo ($o == 6) ? 'selected="selected"' : ''; ?>>6 Monate</option>
								<option value="9" <?php echo ($o == 9) ? 'selected="selected"' : ''; ?>>9 Monate</option>
								<option value="12" <?php echo ($o == 12) ? 'selected="selected"' : ''; ?>>12 Monate</option>
								<option value="18" <?php echo ($o == 18) ? 'selected="selected"' : ''; ?>>18 Monate</option>
								<option value="24" <?php echo ($o == 24) ? 'selected="selected"' : ''; ?>>24 Monate</option>
								<option value="36" <?php echo ($o == 36) ? 'selected="selected"' : ''; ?>>36 Monate</option>
								<option value="48" <?php echo ($o == 48 || !isset($wp_query->query['o'])) ? 'selected="selected"' : ''; ?>>48 Monate</option>
								<option value="60" <?php echo ($o == 60) ? 'selected="selected"' : ''; ?>>60 Monate</option>
								<option value="72" <?php echo ($o == 72) ? 'selected="selected"' : ''; ?>>72 Monate</option>
								<option value="84" <?php echo ($o == 84) ? 'selected="selected"' : ''; ?>>84 Monate</option>
								<option value="96" <?php echo ($o == 96) ? 'selected="selected"' : ''; ?>>96 Monate</option>
								<option value="108" <?php echo ($o == 108) ? 'selected="selected"' : ''; ?>>108 Monate</option>
								<option value="120" <?php echo ($o == 120) ? 'selected="selected"' : ''; ?>>120 Monate</option>
							</select>
						</div>
					</div>
				</form>
			</div>
		</div>

		<div class="row">
			<div class="col-lg-12">
				<div class="divTable table-tagesgeld tbl-desktop">
					<div class="divTableBody">
						<div class="divTableRow header">
							<div class="divTableCell">Anbieter</div>
							<div class="divTableCell">Zinssatz</div>
							<div class="divTableCell">Bemerkungen</div>
							<div class="divTableCell">Zum Anbieter</div>
						</div>

						<?php
							global $wp_query;
							$allowed_vals = array(1,3,6,9,12,18,24,36,48,60,72,84,96,108,120);
							$default = '48_month';
							$cache_key_id = '20201017-6';
							$cache_key = 'festgeld-query-48-'.$cache_key_id;
							if (isset($wp_query->query['o']) && in_array($wp_query->query['o'], $allowed_vals)) {
								$default = $wp_query->query['o'];

								switch ($o) {
									case 1:
										$cache_key = 'festgeld-query-1-'.$cache_key_id;
										$default = '1_month';
										break;
									case 3:
										$cache_key = 'festgeld-query-3-'.$cache_key_id;
										$default = '3_month';
										break;
									case 6:
										$cache_key = 'festgeld-query-6-'.$cache_key_id;
										$default = '6_month';
										break;
									case 9:
										$cache_key = 'festgeld-query-9-'.$cache_key_id;
										$default = '9_month';
										break;
									case 12:
										$cache_key = 'festgeld-query-12-'.$cache_key_id;
										$default = '12_month';
										break;
									case 18:
										$cache_key = 'festgeld-query-18-'.$cache_key_id;
										$default = '18_month';
										break;
									case 24:
										$cache_key = 'festgeld-query-24-'.$cache_key_id;
										$default = '24_month';
										break;
									case 36:
										$cache_key = 'festgeld-query-36-'.$cache_key_id;
										$default = '36_month';
										break;
									case 60:
										$cache_key = 'festgeld-query-60-'.$cache_key_id;
										$default = '60_month';
										break;
									case 72:
										$cache_key = 'festgeld-query-72-'.$cache_key_id;
										$default = '72_month';
										break;
									case 84:
										$cache_key = 'festgeld-query-84-'.$cache_key_id;
										$default = '84_month';
										break;
									case 96:
										$cache_key = 'festgeld-query-96-'.$cache_key_id;
										$default = '96_month';
										break;
									case 108:
										$cache_key = 'festgeld-query-108-'.$cache_key_id;
										$default = '108_month';
										break;
									case 120:
										$cache_key = 'festgeld-query-120-'.$cache_key_id;
										$default = '120_month';
										break;
									default:
										$cache_key = 'festgeld-query-48-'.$cache_key_id;
										$default = '48_month';
										break;
								}
							}

							$qry = "SELECT banks.image,festgeld.man_account_type,festgeld.first_mirror_stroke,festgeld.second_mirror_stroke,festgeld.festgeld_id,banks.link_festgeld,banks.country_name,banks.bank_name,banks.sANDp_rating, banks.statement, festgeld.{$default} AS interest,festgeld.zinssatz_garantie,
								festgeld.1_month AS i1,
								festgeld.3_month AS i3,
								festgeld.6_month AS i6,
								festgeld.9_month AS i9,
								festgeld.12_month AS i12,
								festgeld.18_month AS i18,
								festgeld.24_month AS i24,
								festgeld.36_month AS i36,
								festgeld.48_month AS i48,
								festgeld.60_month AS i60,
								festgeld.72_month AS i72,
								festgeld.84_month AS i84,
								festgeld.96_month AS i96,
								festgeld.108_month AS i108,
								festgeld.120_month AS i120
							 FROM `festgeld` INNER JOIN banks ON banks.bank_id = festgeld.bank_id WHERE banks.tagesgeld_id IS NOT NULL AND banks.status = 'ok' AND (festgeld.1_month > 0
                             OR festgeld.3_month > 0
                             OR festgeld.6_month > 0
                             OR festgeld.9_month > 0
                             OR festgeld.12_month > 0
                             OR festgeld.18_month > 0
                             OR festgeld.24_month > 0
                             OR festgeld.36_month > 0
                             OR festgeld.48_month > 0
                             OR festgeld.60_month > 0
                             OR festgeld.72_month > 0
                             OR festgeld.84_month > 0
                             OR festgeld.96_month > 0
                             OR festgeld.108_month > 0
                             OR festgeld.120_month > 0) ORDER BY interest DESC ";

							 #echo $qry;

							if ( ! $html = get_transient( $cache_key )  ) {
							    $banks = $wpdb->get_results( $qry );
							    ob_start();
							    foreach ($banks as $bank) : ?>
						<div class="divTableRow body">
							<div class="divTableCell cellBorder divTableAnbieter">
								<div class="clearfix">
									<div class="col-lg-6">
										<a href="<?php echo get_home_url().str_replace(' ', '', $bank->link_festgeld); ?>">
											<?php if ($bank->image == '' || !file_exists(get_template_directory() .'/includes/img/logos/' .  $bank->image)) : ?>
												<span class="image-txt"><?php echo str_replace('_', ' ', $bank->bank_name); ?></span>
											<?php else : ?>
											<img src="<?php echo get_template_directory_uri() .'/includes/img/logos/' .  $bank->image; ?>" title="weitere Informationen zum Festgeld der <?php echo str_replace('_', ' ', $bank->bank_name); ?>" class="img-responsive">
											<?php endif; ?>
										</a>
										<?php if ($bank->man_account_type != null || $bank->man_account_type != '') : ?>
										<br>
										<p class="text-left"><?php echo $bank->man_account_type; ?></p>
										<?php endif; ?>
									</div>
									<div class="col-lg-6">
										<h3><?php echo str_replace('_', ' ', $bank->bank_name); ?></h3>
										<ul class="list-unstyled anbieter">
											<?php if ($bank->country_name != '' ) : ?>
											<li><?php echo $bank->country_name; ?></li>
											<?php endif; ?>
											<li>Bonitat: <?php echo $bank->sANDp_rating; ?></li>
											<li><?php echo $bank->statement; ?></li>
										</ul>
									</div>
								</div>
							</div>
							<div class="divTableCell cellBorder zinssatz-row">
								<span>Zinssatz:</span><strong><?php echo number_format($bank->interest, 2, ',', '.'); ?>%</strong><br>
								<a href="#" id="detail" data-id="<?php echo $bank->festgeld_id; ?>">Details</a>
							</div>
							<div class="divTableCell cellBorder bemerkungen-cell">
								<ul class="list-unstyled bemerkungen">
									<li><?php echo $bank->first_mirror_stroke; ?></li>
									<?php if ($bank->zinssatz_garantie != '') : ?>
									<li><?php echo str_replace('Angebotszins gilt für die ersten', 'Zinsgarantie', $bank->zinssatz_garantie); ?></li>
									<?php else : ?>
									<li><?php echo $bank->second_mirror_stroke; ?></li>
									<?php endif; ?>
									<li><a href="<?php echo get_home_url(); ?>/festgeld/<?php echo $bank->bank_name; ?>/">Produktdetails</a></li>
								</ul>
							</div>
							<div class="divTableCell cellBorder">
								<a href="<?php echo get_home_url().str_replace(' ', '', $bank->link_festgeld);?>" class="btn btn-default" onclick="_gaq.push(['_trackPageview', '/vergleich/<?php echo $bank->bank_name; ?>/festgeld/']);return true;" title="zum Festgeld-Angebot der <?php echo str_replace('_', ' ', $bank->bank_name)?>">zur Bank &gt;</a>
							</div>
						</div>
						<div class="divTableRow body caption festgeld-<?php echo $bank->festgeld_id; ?>" style="display: none;" data-target="<?php echo $bank->festgeld_id; ?>">
							<div class="caption-wrapper">
								<div class="caption-inner">
									<div class="row">
										<div class="col-sm-12 col-md-6 col-lg-3">
											<ul class="list-unstyled">
												<li><span>1 Monat</span><?php echo number_format($bank->i1, 2, ',', '.'); ?>%</li>
												<li><span>3 Monate</span><?php echo number_format($bank->i3, 2, ',', '.'); ?>%</li>
												<li><span>6 Monate</span><?php echo number_format($bank->i6, 2, ',', '.'); ?>%</li>
												<li><span>9 Monate</span><?php echo number_format($bank->i9, 2, ',', '.'); ?>%</li>
											</ul>
										</div>
										<div class="col-sm-12 col-md-6 col-lg-3">
											<ul class="list-unstyled">
												<li><span>12 Monate</span><?php echo number_format($bank->i12, 2, ',', '.'); ?>%</li>
												<li><span>24 Monate</span><?php echo number_format($bank->i24, 2, ',', '.'); ?>%</li>
												<li><span>36 Monate</span><?php echo number_format($bank->i36, 2, ',', '.'); ?>%</li>
												<li><span>48 Monate</span><?php echo number_format($bank->i48, 2, ',', '.'); ?>%</li>
											</ul>
										</div>
										<div class="col-sm-12 col-md-6 col-lg-3">
											<ul class="list-unstyled">
												<li><span>60 Monate</span><?php echo number_format($bank->i60, 2, ',', '.'); ?>%</li>
												<li><span>72 Monate</span><?php echo number_format($bank->i72, 2, ',', '.'); ?>%</li>
												<li><span>84 Monate</span><?php echo number_format($bank->i84, 2, ',', '.'); ?>%</li>
												<li><span>96 Monate</span><?php echo number_format($bank->i96, 2, ',', '.'); ?>%</li>
											</ul>
										</div>
										<div class="col-sm-12 col-md-6 col-lg-3">
											<ul class="list-unstyled">
												<li><span>108 Monate</span><?php echo number_format($bank->i108, 2, ',', '.'); ?>%</li>
												<li><span>120 Monate</span><?php echo number_format($bank->i120, 2, ',', '.'); ?>%</li>
											</ul>
										</div>
									</div>
								</div>
							</div>
						</div>
						<?php endforeach;
							$html = ob_get_clean();
							set_transient( $cache_key, $html, 12 * 3600 );
						}
						echo $html;
						?>

					</div>
				</div>
				<?php include(get_template_directory() . '/template-parts/table-festgeld-medium.php');?>

				<?php include(get_template_directory() . '/template-parts/table-festgeld-small.php'); ?>
				<div class="tagesgeld-footer-date text-right">
					<p>Zuletzt aktualisiert: <?php echo date('d. m. Y');?></p>
				</div>
			</div>
		</div>



		<div class="row">
			<div class="col-lg-9">
				<?php
				$args_latest = array(
					'post_type' => 'post',
					'ignore_sticky_posts' => 1,
					'posts_per_page' => 5,
					'category_name' => 'zinsmeldungen'
				);
		        $the_query = new WP_Query($args_latest);
		        ?>
				<?php if (!empty($the_query->posts)) : ?>
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="zinsticker-front text-left">Zins-Ticker</h3>
					</div>
		  			<div class="panel-body panel-news">
		  				<div class="body">
		  					<ul class="list-unstyled">
		  						<?php while ($the_query -> have_posts()) : $the_query -> the_post(); ?>
		  						<li>
		  							<h6><?php echo date('d.m.Y', strtotime(get_the_date()));?></h6>
		  							<?php the_title( '<h3 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h3>');?>
		  						</li>
		  						<?php endwhile; ?>
		  						<?php wp_reset_query();?>
		  					</ul>
		  					<div class="panel-body-footer text-right">
		  						<a href="<?php echo get_home_url(); ?>/category/zinsmeldungen/" class="btn btn-default">Alle Meldungen</a>
		  					</div>
		  				</div>
		  			</div>
		  			<div class="panel-footer"></div>
				</div>
				<?php endif; ?>

				<?php
					$widget_content = '';
					$sidebar_id = 'wissenswertes-f';
					$sidebars_widgets = wp_get_sidebars_widgets();
					$widget_ids = $sidebars_widgets[$sidebar_id];
					foreach( $widget_ids as $id ) {
						$wdgtvar = 'widget_'._get_widget_id_base( $id );
					    $idvar = _get_widget_id_base( $id );
					    $instance = get_option( $wdgtvar );
					    $idbs = str_replace( $idvar.'-', '', $id );
					    $widget_content = $instance[$idbs]['text'];
					    $widget_title = $instance[$idbs]['title'];
					}
				?>
				<div class="panel panel-default wissenswertes">

					<div class="panel-heading">
						<h3 class="zinsticker-front text-left"><?php echo $widget_title; ?></h3>
					</div>
			  		<div class="panel-body panel-news">
			  			<div class="body">
			  				<?php echo $widget_content; ?>
			  			</div>
			  		</div>
			  		<div class="panel-footer"></div>
				</div>
			</div>
			<div class="col-lg-3">
				<?php get_sidebar('festgeldzinsen');?>
			</div>
		</div>
	</div>
</section>
<?php get_footer(); ?>
