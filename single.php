<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Aktuellezinsen.net
 */

get_header();
$custom = get_post_custom();
?>
	<div class="container single">
		<div class="row">
			<?php if (!isset($custom['show_comparison_table']) || $custom['show_comparison_table'][0] == '0') : ?>
			<div class="col-lg-9">
			<?php else : ?>
			<div class="col-lg-12">
			<?php endif; ?>
			<?php
			while ( have_posts() ) :
				the_post();

				get_template_part( 'template-parts/content', get_post_type() );

				//the_post_navigation();

				// If comments are open or we have at least one comment, load up the comment template.
				if ( comments_open() || get_comments_number() ) :
					comments_template();
				endif;

			endwhile; // End of the loop.
			?>
			</div>

			<?php if (!isset($custom['show_comparison_table']) || $custom['show_comparison_table'][0] == '0') : ?>
			<div class="col-lg-3">
				<?php get_sidebar('news'); ?>
			</div>
			<?php endif; ?>
		</div>
	</div>
	
<?php

get_footer();
