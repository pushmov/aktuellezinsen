<?php
global $wp_query;
$name = 'Tagesgeld';

$bankname = $wp_query->query['bankname'];

$data = $wpdb->get_row( "SELECT * FROM `banks` WHERE bank_name = '{$bankname}' AND status = 'ok'" );

if (empty($data)) {
    global $wp_query;
		$wp_query->set_404();
		status_header( 404 );
		get_template_part( 404 );
		exit();
}

$data_tagesgeld = $wpdb->get_row("SELECT * FROM `tagesgeld` WHERE bank_id = '{$data->bank_id}'");
if (empty($data_tagesgeld)) {
    global $wp_query;
		$wp_query->set_404();
		status_header( 404 );
		get_template_part( 404 );
		exit();
}
get_header('tagesgeld-detail');
?>

<section class="ct-news ct-festgeld ct-festgeld-detail">
  <div class="container">
    <div class="row">
      <div class="col-sm-12 col-md-9">
        <h1><?php echo $name . ' ' . str_replace('_', ' ', $data->bank_name);?></h1>
        <ul class="breadcrumb" itemscope itemtype="http://schema.org/BreadcrumbList">
          <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
            <a itemscope itemtype="http://schema.org/Thing" itemprop="item" href="<?php echo get_home_url(); ?>/tagesgeldzinsen/">
              <span itemprop="name">Tagesgeldzinsen Vergleich</span>
            </a>
            <meta itemprop="position" content="1" />
          </li>
          <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
            <a itemscope itemtype="http://schema.org/Thing" itemprop="item" href="<?php echo get_home_url(); ?>/tagesgeld/<?php echo $data->bank_name; ?>/">
              <span itemprop="name"><?php echo str_replace('_', ' ', $data->bank_name);?></span>
            </a>
            <meta itemprop="position" content="2" />
          </li>
        </ul>
        <div class="summary">
          <ul class="list-unstyled">
            <li>Jetzt zu <strong><?php echo number_format($data_tagesgeld->Zinssatz_table, 2, ',', '.'); ?>%</strong> p.a. Tagesgeldzinsen</li>
            <li><?php echo $data->Einlagensicherung_detail; ?></li>
            <?php if ($data_tagesgeld->anlagebetrag_maximum != '0') : ?>
            <li>Anlagebetrag bis zu <?php echo number_format($data_tagesgeld->anlagebetrag_maximum, 0, ',', '.');?> &euro;</li>
            <?php endif; ?>
          </ul>
          <p><?php echo $data->ueber_bank; ?></p>
        </div>

        <div class="info-box">
          <h3>Angebotsdetails</h3>
          <table class="table" width="100%" border="0">
            <tr>
              <td>Anlagebetrag Minimum:</td>
              <td>
              <?php
                $minVal = $data_tagesgeld->man_anlagebetrag_minimum;
                if ($minVal == null || $minVal == '') {
                  $minVal = $data_tagesgeld->anlagebetrag_minimum;
                }
                echo number_format($minVal, 0, ',', '.');
                ?> &euro;
              </td>
            </tr>
            <tr>
              <td>Anlagebetrag Maximum:</td>
              <td><?php
                $maxVal = $data_tagesgeld->man_anlagebetrag_maximum;
                if ($maxVal == null || $maxVal == '') {
                  $maxVal = $data_tagesgeld->anlagebetrag_maximum;
                }
                echo ($maxVal == '0' ? 'n.a.' : number_format($maxVal, 0, ',', '.').' &euro;');
                ?></td>
            </tr>
            <tr>
              <td>Zinstermine:</td>
              <td><?php echo ($data_tagesgeld->Zinstermine == '' ? 'n.a.' : $data_tagesgeld->Zinstermine); ?></td>
            </tr>
            <?php if ($data_tagesgeld->besonderheit != '') : ?>
            <tr>
              <td>Bemerkungen:</td>
              <td><?php echo $data_tagesgeld->besonderheit; ?></td>
            </tr>
            <?php endif; ?>
            <tr>
              <td>Einlagensicherung:</td>
              <td><?php echo $data->Einlagensicherung; ?></td>
            </tr>
            <tr>
              <td>Zinssatz:</td>
              <td><?php echo ($data_tagesgeld->zinssatz_garantie == '') ? 'variabel' : $data_tagesgeld->zinssatz_garantie; ?></td>
            </tr>
          </table>
        </div>

        <h3 class="description-title">Einlagensicherung <?php echo $data->country_name; ?></h3>
        <div class="summary description">
        <p><?php echo $data->Einlagensicherung_description; ?></p>
        </div>
      </div>
      <div class="col-sm-12 col-md-3">
        <div class="bank-info">
          <?php if ($data->image == '' || !file_exists(get_template_directory() .'/includes/img/logos/' .  $data->image)) : ?>
            <span class="image-txt"><?php echo str_replace('_', ' ', $data->bank_name); ?></span>
          <?php else : ?>
            <img src="<?php echo get_template_directory_uri() .'/includes/img/logos/' .  $data->image; ?>" title="<?php echo str_replace('_', ' ', $data->bank_name); ?>" class="img-responsive">
          <?php endif; ?>
          <ul class="list-unstyled">
            <li>aus <?php echo $data->country_name; ?></li>
            <li>Bonitat: <?php echo $data->sANDp_rating; ?></li>
            <?php if ($data->kontakt != '') : ?>
            <li>Tel: <?php echo $data->kontakt; ?></li>
            <?php endif; ?>
          </ul>
          <a href="<?php echo get_home_url().str_replace(' ', '', $data->link_tagesgeld); ?>" class="btn btn-default" onclick="_gaq.push(['_trackPageview', '/detailseite/<?php echo $data->bank_name; ?>/tagesgeld/']);return true;">zur Bank ></a>
        </div>

        <div class="panel panel-default top-5-festgeld zinssatz">
          <div class="panel-heading">
            <h3>Zinssätze</h3>
          </div>
          <div class="panel-body panel-news">
            <div class="body">
              <ul class="list-unstyled">
                <li>
                  <div class="clearfix">
                    <span>Zinssatz</span><strong class="pull-right"><?php echo number_format($data_tagesgeld->Zinssatz_table, 2, ',', '.');?>%</strong>
                  </div>
                </li>
              </ul>
              <div class="panel-body-footer text-right">
                <a href="<?php echo get_home_url().str_replace(' ', '', $data->link_tagesgeld); ?>" class="btn btn-default" onclick="_gaq.push(['_trackPageview', '/detailseite/<?php echo $data->bank_name; ?>/tagesgeld/']);return true;">jetzt anlegen ></a>
              </div>
            </div>
          </div>
          <div class="panel-footer"></div>
        </div>

      </div>
    </div>
  </div>
</section>
<?php get_footer(); ?>
