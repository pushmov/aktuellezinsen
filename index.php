<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Aktuellezinsen.net
 */

get_header();
?>
	
	<section class="ct-body">
		<div class="container">
			<div class="row">Wir finden die Besten Zinsen</div>
		</div>
	</section>
	<section class="ct-top">
		<div class="container">
			<div class="row">Aktuelle Zinsen</div>
		</div>
	</section>
	<section class="ct-testimonials">
		<div class="container">
			<div class="row">Wir helfen Ihnen, die richtigen Entscheidungen zu treffen</div>
		</div>
	</section>
	<section class="ct-news">
		<div class="container">
			<div class="row">
				<div class="col-lg-4">
					<div class="panel panel-default">
						<div class="panel-heading">Panel Bank template</div>
		  				<div class="panel-body">
		  					<div class="header clearfix">
		  						<div class="anbieter pull-left">Anbieter</div>
		  						<div class="zinssatz pull-left">Zinssatz</div>
		  					</div>
		  					<div class="body">
		  						<div class="item clearfix">
			  						<div class="anbieter pull-left">
			  							<img src="<?php echo get_template_directory_uri(); ?>/includes/img/logos/SWK_Bank-sm.png" class="img-responsive">
			  						</div>
			  						<div class="zinssatz pull-left">Zinssatz</div>
		  						</div>
		  						<div class="item clearfix">
			  						<div class="anbieter pull-left">
			  							<img src="<?php echo get_template_directory_uri(); ?>/includes/logos/Ziraat-sm.png" class="img-responsive">
			  						</div>
			  						<div class="zinssatz pull-left">Zinssatz</div>
		  						</div>
		  						<div class="item clearfix">
			  						<div class="anbieter pull-left">
			  							<img src="<?php echo get_template_directory_uri(); ?>/includes/logos/amsterdam-trade-bank-sm.png" class="img-responsive">
			  						</div>
			  						<div class="zinssatz pull-left">Zinssatz</div>
		  						</div>
		  						<div class="item clearfix">
			  						<div class="anbieter pull-left">
			  							<img src="<?php echo get_template_directory_uri(); ?>/includes/logos/klarna_logo-sm.png" class="img-responsive">
			  						</div>
			  						<div class="zinssatz pull-left">Zinssatz</div>
		  						</div>
		  						<div class="item clearfix">
			  						<div class="anbieter pull-left">
			  							<img src="<?php echo get_template_directory_uri(); ?>/includes/logos/hoist_finance-sm.png" class="img-responsive">
			  						</div>
			  						<div class="zinssatz pull-left">Zinssatz</div>
		  						</div>
		  						<div class="panel-body-footer text-right">
		  							<button class="btn btn-default">Alle Anbieter anzeigen</button>
		  						</div>
		  					</div>
		  				</div>
		  				<div class="panel-footer"></div>
					</div>
				</div>
				<div class="col-lg-4">
					<div class="panel panel-default">
						<div class="panel-heading">Panel Bank template</div>
		  				<div class="panel-body">
		  					<div class="header clearfix">
		  						<div class="anbieter pull-left">Anbieter</div>
		  						<div class="zinssatz pull-left">Zinssatz</div>
		  					</div>
		  					<div class="body">
		  						<div class="item clearfix">
			  						<div class="anbieter pull-left">
			  							<img src="<?php echo get_template_directory_uri(); ?>/includes/logos/SWK_Bank-sm.png" class="img-responsive">
			  						</div>
			  						<div class="zinssatz pull-left">Zinssatz</div>
		  						</div>
		  						<div class="item clearfix">
			  						<div class="anbieter pull-left">
			  							<img src="<?php echo get_template_directory_uri(); ?>/includes/logos/Ziraat-sm.png" class="img-responsive">
			  						</div>
			  						<div class="zinssatz pull-left">Zinssatz</div>
		  						</div>
		  						<div class="item clearfix">
			  						<div class="anbieter pull-left">
			  							<img src="<?php echo get_template_directory_uri(); ?>/includes/logos/amsterdam-trade-bank-sm.png" class="img-responsive">
			  						</div>
			  						<div class="zinssatz pull-left">Zinssatz</div>
		  						</div>
		  						<div class="item clearfix">
			  						<div class="anbieter pull-left">
			  							<img src="<?php echo get_template_directory_uri(); ?>/includes/logos/klarna_logo-sm.png" class="img-responsive">
			  						</div>
			  						<div class="zinssatz pull-left">Zinssatz</div>
		  						</div>
		  						<div class="item clearfix">
			  						<div class="anbieter pull-left">
			  							<img src="<?php echo get_template_directory_uri(); ?>/includes/logos/hoist_finance-sm.png" class="img-responsive">
			  						</div>
			  						<div class="zinssatz pull-left">Zinssatz</div>
		  						</div>
		  						<div class="panel-body-footer text-right">
		  							<button class="btn btn-default">Alle Anbieter anzeigen</button>
		  						</div>
		  					</div>
		  				</div>
		  				<div class="panel-footer"></div>
					</div>
				</div>
				<div class="col-lg-4">
					<div class="panel panel-default">
						<div class="panel-heading">Panel news template</div>
		  				<div class="panel-body panel-news">
		  					<div class="body">Panel content</div>
		  				</div>
		  				<div class="panel-footer"></div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<div id="primary" class="content-area hide">
		<main id="main" class="site-main">

		<?php
		if ( have_posts() ) :

			if ( is_home() && ! is_front_page() ) :
				?>
				<header>
					<h1 class="page-title screen-reader-text"><?php single_post_title(); ?></h1>
				</header>
				<?php
			endif;

			/* Start the Loop */
			while ( have_posts() ) :
				the_post();

				/*
				 * Include the Post-Type-specific template for the content.
				 * If you want to override this in a child theme, then include a file
				 * called content-___.php (where ___ is the Post Type name) and that will be used instead.
				 */
				get_template_part( 'template-parts/content', get_post_type() );

			endwhile;

			the_posts_navigation();

		else :

			get_template_part( 'template-parts/content', 'none' );

		endif;
		?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_sidebar();
get_footer();
