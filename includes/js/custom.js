
    jQuery(document).ready(function () {
        jQuery('ul.dropdown-menu [data-toggle=dropdown]').on('click', function (event) {
            event.preventDefault();
            event.stopPropagation();
            jQuery(this).parent().siblings().removeClass('open');
            jQuery(this).parent().toggleClass('open');
        });

        if (jQuery('.selectpicker').length > 0) {
        	jQuery('.selectpicker').selectpicker({
			  style: 'btn-info',
			  size: 'auto'
			});
			jQuery('.selectpicker').on('changed.bs.select', function (e) {
				var url = window.location.href;
				url = jQuery(this).attr('data-href') + '?o=' + jQuery('.selectpicker').val();
                if (jQuery('.selectpicker').val() === '48') {
                    url = jQuery(this).attr('data-href');
                }
				window.location.href = url;
			});
        }

        jQuery('.zinssatz-row a').on('click', function(e){
            e.preventDefault();
            var l = jQuery(this);
            
            jQuery('.festgeld-'+jQuery(this).attr('data-id')).toggle(250, function(){
                if (jQuery(this).is(':hidden')) {
                    l.closest('.divTableRow').find('.cellBorder').each(function(){
                        jQuery(this).css({
                            'border': '2px solid #fff'
                        });
                    });
                    l.closest('.divTableRow').find('.cellBorder:first').css({
                        'border-left': 'none'
                    });
                    l.closest('.divTableRow').find('.cellBorder:last').css({
                        'border-right': 'none'
                    });
                    l.removeClass('up');
                } else {
                    l.closest('.divTableRow').find('.cellBorder').each(function(){
                        jQuery(this).css({
                            'border-left': '2px solid transparent',
                            'border-right': '2px solid transparent',
                            'border-bottom': '2px solid transparent'
                        });
                    });
                    l.closest('.divTableRow').find('.cellBorder:first').css({
                        'border-left': 'none'
                    });
                    l.closest('.divTableRow').find('.cellBorder:last').css({
                        'border-right': 'none'
                    });
                    l.addClass('up');
                }
            });
            
        });

        jQuery('.tbl-small .detail').on('click', function(e){
            e.preventDefault();
            jQuery(this).parent().find('.small-detail').toggleClass('hide', 5000);
        });

        jQuery('.tbl-mobile .detail').on('click', function(e){
            e.preventDefault();
            jQuery(this).parent().find('.small-detail').toggleClass('hide', 5000);
        });

        


    });
