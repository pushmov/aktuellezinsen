<?php
// Creating the widget 
class aktuellenews_widget extends WP_Widget {
 
	function __construct() {
		parent::__construct(
	 
		// Base ID of your widget
			'aktuellenews_widget', 
	 
			// Widget name will appear in UI
			__('Aktuelle News', 'aktuellenews_widget_domain'), 
	 
			// Widget description
			array( 'description' => __( 'Widget For Aktuelle News', 'aktuellenews_widget_domain' ), ) 
		);
	}


	public function widget( $args, $instance ) {
		$title = apply_filters( 'widget_title', $instance['post_id'] );
		 
		// before and after widget arguments are defined by themes
		echo $args['before_widget'];
		if ( ! empty( $title ) )
		echo $args['before_title'] . $title . $args['after_title'];
		 
		// This is where you run the code and display the output
		echo __( 'Hello, World!', 'aktuellenews_widget_domain' );
		echo $args['after_widget'];
	}
	         
	// Widget Backend 
	public function form( $instance ) {
		if ( isset( $instance[ 'headline' ] ) ) {
			$headline = $instance[ 'headline' ];
		}
		else {
			$headline = __( 'Aktuelle News', 'aktuellenews_widget_domain' );
		}
		if ( isset( $instance[ 'post_id' ] ) ) {
			$post_id = $instance[ 'post_id' ];
		}
		else {
			$post_id = __( 'Post or Page ID', 'aktuellenews_widget_domain' );
		}
		if ( isset( $instance[ 'post_id_2' ] ) ) {
			$post_id_2 = $instance[ 'post_id_2' ];
		}
		else {
			$post_id_2 = __( 'Post or Page ID', 'aktuellenews_widget_domain' );
		}
		if ( isset( $instance[ 'post_id_3' ] ) ) {
			$post_id_3 = $instance[ 'post_id_3' ];
		}
		else {
			$post_id_3 = __( 'Post or Page ID', 'aktuellenews_widget_domain' );
		}
		// Widget admin form
		?>
		<p>
			<label for="<?php echo $this->get_field_id( 'headline' ); ?>"><?php _e( 'Headline:' ); ?></label> 
			<input class="widefat" id="<?php echo $this->get_field_id( 'headline' ); ?>" name="<?php echo $this->get_field_name( 'headline' ); ?>" type="text" value="<?php echo esc_attr( $headline ); ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'post_id' ); ?>"><?php _e( 'Entry #1:' ); ?></label> 
			<input class="widefat" id="<?php echo $this->get_field_id( 'post_id' ); ?>" name="<?php echo $this->get_field_name( 'post_id' ); ?>" type="text" value="<?php echo esc_attr( $post_id ); ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'post_id_2' ); ?>"><?php _e( 'Entry #2:' ); ?></label> 
			<input class="widefat" id="<?php echo $this->get_field_id( 'post_id_2' ); ?>" name="<?php echo $this->get_field_name( 'post_id_2' ); ?>" type="text" value="<?php echo esc_attr( $post_id_2 ); ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'post_id_3' ); ?>"><?php _e( 'Entry #3:' ); ?></label> 
			<input class="widefat" id="<?php echo $this->get_field_id( 'post_id_3' ); ?>" name="<?php echo $this->get_field_name( 'post_id_3' ); ?>" type="text" value="<?php echo esc_attr( $post_id_3 ); ?>" />
		</p>
		<?php 
	}
	     
	// Updating widget replacing old instances with new
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['headline'] = ( ! empty( $new_instance['headline'] ) ) ? strip_tags( $new_instance['headline'] ) : '';
		$instance['post_id'] = ( ! empty( $new_instance['post_id'] ) ) ? strip_tags( $new_instance['post_id'] ) : '';
		$instance['post_id_2'] = ( ! empty( $new_instance['post_id_2'] ) ) ? strip_tags( $new_instance['post_id_2'] ) : '';
		$instance['post_id_3'] = ( ! empty( $new_instance['post_id_3'] ) ) ? strip_tags( $new_instance['post_id_3'] ) : '';
		return $instance;
	}
}