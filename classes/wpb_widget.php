<?php
// Creating the widget 
class wpb_widget extends WP_Widget {
 
	function __construct() {
		parent::__construct(
	 
		// Base ID of your widget
			'wpb_widget', 
	 
			// Widget name will appear in UI
			__('Bank', 'wpb_widget_domain'), 
	 
			// Widget description
			array( 'description' => __( 'Widget Custom Table', 'wpb_widget_domain' ), ) 
		);
	}


	public function widget( $args, $instance ) {
		$title = apply_filters( 'widget_title', $instance['title'] );
		 
		// before and after widget arguments are defined by themes
		echo $args['before_widget'];
		if ( ! empty( $title ) )
		echo $args['before_title'] . $title . $args['after_title'];
		 
		// This is where you run the code and display the output
		echo __( 'Hello, World!', 'wpb_widget_domain' );
		echo $args['after_widget'];
	}
	         
	// Widget Backend 
	public function form( $instance ) {
		if ( isset( $instance[ 'title' ] ) ) {
			$title = $instance[ 'title' ];
		}
		else {
			$title = __( 'Bank Name', 'wpb_widget_domain' );
		}
		if ( isset( $instance[ 'interest' ] ) ) {
			$interest = $instance[ 'interest' ];
		}
		else {
			$interest = __( '0%', 'wpb_widget_domain' );
		}
		if ( isset( $instance[ 'target' ] ) ) {
			$target = $instance[ 'target' ];
		}
		else {
			$target = __( 'http://', 'wpb_widget_domain' );
		}
		if ( isset( $instance[ 'onclick' ] ) ) {
			$onclick = $instance[ 'onclick' ];
		}
		else {
			$onclick = __( '', 'wpb_widget_domain' );
		}
		// Widget admin form
		?>
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Bank:' ); ?></label> 
			<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'interest' ); ?>"><?php _e( 'Interest:' ); ?></label> 
			<input class="widefat" id="<?php echo $this->get_field_id( 'interest' ); ?>" name="<?php echo $this->get_field_name( 'interest' ); ?>" type="text" value="<?php echo esc_attr( $interest ); ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'target' ); ?>"><?php _e( 'Target:' ); ?></label> 
			<input class="widefat" id="<?php echo $this->get_field_id( 'target' ); ?>" name="<?php echo $this->get_field_name( 'target' ); ?>" type="text" value="<?php echo esc_attr( $target ); ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'onclick' ); ?>"><?php _e( 'Onclick:' ); ?></label> 
			<textarea class="widefat" id="<?php echo $this->get_field_id( 'onclick' ); ?>" name="<?php echo $this->get_field_name( 'onclick' ); ?>"><?php echo esc_attr( $onclick ); ?></textarea>
		</p>
		<?php 
	}
	     
	// Updating widget replacing old instances with new
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		$instance['interest'] = ( ! empty( $new_instance['interest'] ) ) ? strip_tags( $new_instance['interest'] ) : '';
		$instance['target'] = ( ! empty( $new_instance['target'] ) ) ? strip_tags( $new_instance['target'] ) : '';
		$instance['onclick'] = ( ! empty( $new_instance['onclick'] ) ) ? strip_tags( $new_instance['onclick'] ) : '';
		return $instance;
	}
}