<?php
// Creating the widget 
global $wpdb;
class topfestgeld_widget extends WP_Widget {
 
	function __construct() {
		parent::__construct(
	 
		// Base ID of your widget
			'topfestgeld_widget', 
	 
			// Widget name will appear in UI
			__('Top 5 Festgeld', 'topfestgeld_widget_domain'), 
	 
			// Widget description
			array( 'description' => __( 'Top 5 Festgeld Widget', 'topfestgeld_widget_domain' ), ) 
		);
	}


	public function widget( $args, $instance ) {
		global $wpdb;
		$title = apply_filters( 'widget_title', $instance['title'] );
		// before and after widget arguments are defined by themes 
		// This is where you run the code and display the output
		$result = $wpdb->get_results(" SELECT banks.bank_name, festgeld.48_month AS interest,banks.link_festgeld FROM `festgeld` INNER JOIN banks ON banks.bank_id = festgeld.bank_id WHERE banks.status = 'ok' ORDER BY 48_month DESC LIMIT 0,5");
		$data['title'] = $title;
		$data['ct'] = $result;
		include(get_template_directory().'/template-parts/widget-top-festgeldzinsen.php');
	}
	         
	// Widget Backend 
	public function form( $instance ) {
		global $wpdb;
		$result = $wpdb->get_results(" SELECT banks.bank_name, festgeld.48_month AS interest FROM `festgeld` INNER JOIN banks ON banks.bank_id = festgeld.bank_id WHERE banks.status = 'ok' ORDER BY 48_month DESC LIMIT 0,5");
		if ( isset( $instance[ 'title' ] ) ) {
			$title = $instance[ 'title' ];
		}
		else {
			$title = __( 'Headline', 'topfestgeld_widget_domain' );
		}
		?>
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label> 
			<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
		</p>
		<ul>
			<?php foreach ($result as $row) :?>
			<li><span style="width: 120px; display: inline-block;"><?php echo str_replace('_', ' ', $row->bank_name); ?></span> <?php echo number_format($row->interest, 2, ',', '.'); ?>%</li>
			<?php endforeach; ?>
		</ul>
		<?php
	}
	     
	// Updating widget replacing old instances with new
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		return $instance;
	}
}