<?php
// Creating the widget 
class author_widget extends WP_Widget {
 
	function __construct() {
		parent::__construct(
	 
		// Base ID of your widget
			'author_widget', 
	 
			// Widget name will appear in UI
			__('Author Display Name', 'zinsticker_widget_domain'), 
	 
			// Widget description
			array( 'description' => __( 'Widget Custom Author', 'author_widget_domain' ), ) 
		);
	}


	public function widget( $args, $instance ) {
		$title = apply_filters( 'widget_title', $instance['title_author_name'] );
		 
		// before and after widget arguments are defined by themes
		echo $args['before_widget'];
		if ( ! empty( $title ) )
		// This is where you run the code and display the output
		//include(get_template_directory().'/template-parts/widget-zinsticker.php');
		echo 'a';
	}
	         
	// Widget Backend 
	public function form( $instance ) {
		
		if ( isset( $instance[ 'title_author_name' ] ) ) {
			$title_author_name = $instance[ 'title_author_name' ];
		}
		else {
			$title_author_name = __( 'Display Name', 'author_widget_domain' );
		}
		
		?>

		<p>
			<label for="<?php echo $this->get_field_id( 'title_author_name' ); ?>"><?php _e( 'Display Name:' ); ?></label> 
			<input class="widefat" id="<?php echo $this->get_field_id( 'title_author_name' ); ?>" name="<?php echo $this->get_field_name( 'title_author_name' ); ?>" type="text" value="<?php echo esc_attr( $title_author_name ); ?>" />
		</p>
		
		<?php 
	}
	     
	// Updating widget replacing old instances with new
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title_author_name'] = ( ! empty( $new_instance['title_author_name'] ) ) ? strip_tags( $new_instance['title_author_name'] ) : '';
		$instance['author_display_name'] = ( ! empty( $new_instance['author_display_name'] ) ) ? strip_tags( $new_instance['author_display_name'] ) : '';
		return $instance;
	}
}